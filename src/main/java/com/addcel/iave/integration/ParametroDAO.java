package com.addcel.iave.integration;

import com.addcel.iave.utils.U;
import com.addcel.iave.controller.IaveServiciosController;

import crypto.Crypto;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import org.springframework.web.servlet.ModelAndView;

import com.addcel.iave.model.mapper.IaveServiciosMapper;
import com.addcel.iave.servicios.services.IaveServiciosService;

import org.apache.log4j.Logger;

/**
 *
 * @author JCDP
 */

@Service
public class ParametroDAO {
    
	@Autowired
	private IaveServiciosMapper antadServiciosMapper;
	
	public ParametroDAO() {

	}
        private IaveServiciosController asc = new IaveServiciosController();
	
	private static final Logger logger = Logger.getLogger(ParametroDAO.class);

    private static final String TelefonoServicio="5525963513";

    public static String setSMS(String Telefono){
        return Crypto.aesEncrypt(parsePass(TelefonoServicio),  Telefono);    
    }
    public static String getSMS(String Telefono){
        return Crypto.aesDecrypt(parsePass(TelefonoServicio),  Telefono);    
    }

    public void EnviaMailBGDF(String Usuario, String Password, String Mail){
        try{
            String jsonObject = "{\"usuario\":\""+ Usuario + "\",\"contrasena\":\"" + Password + "\"}";
            StringBuffer paramEmail = new StringBuffer();
            paramEmail.append("correo=").append(Mail);
            paramEmail.append("&json=").append(jsonObject);
            String respuesta = peticionUrlPostParams( "http://50.57.192.214:8080/MailSenderAddcel/envia-bienvenida-gdf/", paramEmail.toString());
            if(respuesta.equals("OK")){
                logger.debug("EnviaMail: OK");
            }        
        }catch(Exception e){
            logger.error("EnviaMail: " + e.toString());
        }
    }
    
    private static String peticionUrlPostParams(String urlRemota,String parametros) throws IOException {
        String respuesta = null;
        URL url = new URL(urlRemota);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        // Se indica que se escribira en la conexión
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        // Se escribe los parametros enviados a la url
        OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
        wr.write(parametros);
        wr.close();			
        if(con.getResponseCode() == HttpURLConnection.HTTP_OK ||
            con.getResponseCode() == HttpURLConnection.HTTP_ACCEPTED){
            respuesta = "OK";
        }else{
            respuesta = "ERROR";
        }
        con.disconnect();

        return respuesta;
    }


   
   public String EnviaMail(String Nombre,  String Mail, String TipoMensaje, String Asunto, String Usuario, 
            String Password, String Monto, String Autorizacion, String Comision, String Saldo, IaveServiciosService antadService){

        try{
            java.util.Date fecha = new java.util.Date();

            //String wAsunto = new ParametroDAO().getParametro(Asunto);
            //String Cadena = new ParametroDAO().getParametro(TipoMensaje);
            logger.debug("antes de obtener datos para mail ");
            String wAsunto = Asunto;
            String Cadena = TipoMensaje;
            logger.debug("despues de obtener datos para mail ");
            logger.debug("wAsunto : " + wAsunto );
            logger.debug("Cadena : " + Cadena );
            
            Cadena = Cadena.replace("@NOMBRE", Nombre);
            Cadena = Cadena.replace("@USUARIO", Usuario);
            Cadena = Cadena.replace("@PASSWORD", Password);
            Cadena = Cadena.replace("@MONTO", Monto);
            Cadena = Cadena.replace("@AUTORIZACION", Autorizacion);
            Cadena = Cadena.replace("@COMISION", Comision);
            Cadena = Cadena.replace("@FECHA", fecha.toString());
            Cadena = Cadena.replace("@SALDO", Saldo);

            if(Cadena.indexOf("@CARGO") >= 0 ){
                double Car = Double.parseDouble(Monto) +  Double.parseDouble(Comision);
                Cadena = Cadena.replace("@CARGO", "" +Car);
            }

            logger.debug("ENVIANDO MAIL" );

            //boolean x = TagServiciosService.sendMail(Mail, wAsunto ,Cadena, antadService);

            /*if(x){
                logger.debug("EL MAIL SE ENVIO" );
                return "0";

            }else{
                logger.debug("EL MAIL NO SE ENVIO" );
                return "-1";
            }*/
            return "";
        }catch(Exception e){
            logger.error("EnviaMail: " + e.toString());
            return "-1";
        }
    }
    
    /*public String EnviaMailVIAPASS(String Nombre,  String Mail, String TipoMensaje, String Asunto, String Usuario, 
                                   String Password, String Monto, String Autorizacion, String Comision, String Saldo, String AutorizacionVIAPASS){

        try{

            java.util.Date fecha = new java.util.Date();
            String wAsunto = new ParametroDAO().getParametro(Asunto);

            String Cadena = new ParametroDAO().getParametro(TipoMensaje);
            Cadena = Cadena.replace("@NOMBRE", Nombre);
            Cadena = Cadena.replace("@USUARIO", Usuario);
            Cadena = Cadena.replace("@PASSWORD", Password);
            Cadena = Cadena.replace("@MONTO", Monto);
          //  Cadena = Cadena.replace("@AUTORIZACION_VIAPASS", AutorizacionVIAPASS);
            Cadena = Cadena.replace("@AUTORIZACION", Autorizacion);
            Cadena = Cadena.replace("@COMISION", Comision);
            Cadena = Cadena.replace("@FECHA", fecha.toString());
            Cadena = Cadena.replace("@SALDO", Saldo);

            if(Cadena.indexOf("@CARGO") >= 0 ){
                double Car = Double.parseDouble(Monto) +  Double.parseDouble(Comision);
                Cadena = Cadena.replace("@CARGO", "" +Car);
            }

            boolean x= U.sendMail(Mail, wAsunto ,Cadena);

            if(x){
                return "0";
            }else
            {
                return "-1";
            }
        }catch(Exception e){
            logger.error("EnviaMail: " + e.toString());
            return "-1";
        }
    }*/
    
    /*public String EnviaMailOHL(String Nombre,  String Mail, String TipoMensaje, String Asunto, String Usuario, 
                                   String Password, String Monto, String Autorizacion, String Comision, String Saldo, String AutorizacionOHL){

        try{

           java.util.Date fecha = new java.util.Date();

            String wAsunto = new ParametroDAO().getParametro(Asunto);

            String Cadena = new ParametroDAO().getParametro(TipoMensaje);
            Cadena = Cadena.replace("@NOMBRE", Nombre);
            Cadena = Cadena.replace("@USUARIO", Usuario);
            Cadena = Cadena.replace("@PASSWORD", Password);
            Cadena = Cadena.replace("@MONTO", Monto);
          //  Cadena = Cadena.replace("@AUTORIZACION_OHL", AutorizacionOHL);
            Cadena = Cadena.replace("@AUTORIZACION", Autorizacion);
            Cadena = Cadena.replace("@COMISION", Comision);
            Cadena = Cadena.replace("@FECHA", fecha.toString());
            Cadena = Cadena.replace("@SALDO", Saldo);

            if(Cadena.indexOf("@CARGO") >= 0 ){
                double Car = Double.parseDouble(Monto) +  Double.parseDouble(Comision);
                Cadena = Cadena.replace("@CARGO", "" +Car);
            }
            boolean x= U.sendMail(Mail, wAsunto ,Cadena);

            if(x){
                return "0";
            }else
            {
                return "-1";
            }
        }catch(Exception e){
            logger.error("EnviaMail: " + e.toString());
            return "-1";
        }
    }*/
    
    /*public String EnviaMailVitaMedica(String Nombre,  String Mail, String TipoMensaje, String Asunto, 
                                     String Usuario, String Password, String Monto, String Autorizacion, 
                                     String Comision, String Saldo, String NombreTitular,
                                     String FolioVitaMedica, String NumTransaccionBancaria,
                                     String FechaCompra, String HoraCompra){
        try{

            java.util.Date fecha = new java.util.Date();
            String wAsunto =  new ParametroDAO().getParametro(Asunto);

            String Cadena = crypto.Crypto.replaceConAcento( new ParametroDAO().getParametro(TipoMensaje) );
            Cadena = Cadena.replace("@USUARIO", Usuario);
            Cadena = Cadena.replace("@PASSWORD", Password);
            Cadena = Cadena.replace("@IMPORTE", Monto);
            Cadena = Cadena.replace("@AUTORIZACION", Autorizacion);
            Cadena = Cadena.replace("@COMISION", Comision);
            Cadena = Cadena.replace("@SALDO", Saldo);
            Cadena = Cadena.replace("@NOMBRETITULARMEMBRESIA", crypto.Crypto.replaceConAcento(NombreTitular));
            Cadena = Cadena.replace("@FOLIOVITAMEDICA", FolioVitaMedica);
            Cadena = Cadena.replace("@NUMTRANSACCIONBANCARIA", NumTransaccionBancaria);
            Cadena = Cadena.replace("@FECHACOMPRA", FechaCompra.substring(8,10) + "-" + FechaCompra.substring(5,7) + "-" + FechaCompra.substring(0,4));
            Cadena = Cadena.replace("@HORACOMPRA", FechaCompra.substring(11,19));

            logger.debug("EnviaMail: NOMBRE: " + NombreTitular);

            logger.debug("EnviaMail: NOMBRE [LIMPIO]: " + crypto.Crypto.replaceConAcento(NombreTitular));

            Cadena = Cadena.replace("@FECHA", fecha.toString());
            Cadena = Cadena.replace("@NOMBRE", crypto.Crypto.replaceConAcento(Nombre));


            if(Cadena.indexOf("@CARGO") >= 0 ){
                double Car = Double.parseDouble(Monto) +  Double.parseDouble(Comision);
                Cadena = Cadena.replace("@CARGO", "" +Car);
            }

            boolean x= U.sendMail(Mail, wAsunto ,Cadena);

            if(x){
                return "0";
            }else
            {
                return "-1";
            }
        }catch(Exception e){
            logger.error("EnviaMail: " + e.toString());
            return "-1";
        }
    }*/
    
    public static String parsePass(String pass){
        int len = pass.length();
        String key = "";

        for (int i =0; i < 32 /len; i++){
              key += pass;
        }

        int carry = 0;
        while (key.length() < 32){
              key += pass.charAt(carry);
              carry++;
        }
        return key;
    }

    
public String[]  Asf(String cadena){
	
	String Key="";
	String cadenaCompleta="";
	String[] Res= new String [3];
        
	int Long = Integer.parseInt(cadena.substring(0, 2));
	
	//logger.debug("Longuitud: " + String.valueOf(Long));
	//logger.debug("Longuitud cadena: " + String.valueOf(cadena.length()));
	
	cadenaCompleta = cadena.substring(2);
	//logger.debug( cadenaCompleta);
	if(Long <= 8){
		Key = cadena.substring(21,23) + cadena.substring(25,27) + cadena.substring(29,31) + cadena.substring(33,35);
		cadenaCompleta = cadenaCompleta.substring(0,19) + cadenaCompleta.substring(21);
		cadenaCompleta = cadenaCompleta.substring(0,21) + cadenaCompleta.substring(23);
		cadenaCompleta = cadenaCompleta.substring(0,23) + cadenaCompleta.substring(25);
		cadenaCompleta = cadenaCompleta.substring(0,25) + cadenaCompleta.substring(27);
	}else if(Long == 9){
		Key = cadena.substring(21,23) + cadena.substring(25,27) + cadena.substring(29,31) + cadena.substring(33,35);
		Key += cadena.substring(37,38);
		cadenaCompleta = cadenaCompleta.substring(0,19) + cadenaCompleta.substring(21);
		cadenaCompleta = cadenaCompleta.substring(0,21) + cadenaCompleta.substring(23);
		cadenaCompleta = cadenaCompleta.substring(0,23) + cadenaCompleta.substring(25);
		cadenaCompleta = cadenaCompleta.substring(0,25) + cadenaCompleta.substring(27);
		cadenaCompleta = cadenaCompleta.substring(0,27) + cadenaCompleta.substring(28);
	}else if(Long == 10){
		Key = cadena.substring(21,23) + cadena.substring(25,27) + cadena.substring(29,31) + cadena.substring(33,35);
		Key += cadena.substring(37,39);
		cadenaCompleta = cadenaCompleta.substring(0,19) + cadenaCompleta.substring(21);
		cadenaCompleta = cadenaCompleta.substring(0,21) + cadenaCompleta.substring(23);
		cadenaCompleta = cadenaCompleta.substring(0,23) + cadenaCompleta.substring(25);
		cadenaCompleta = cadenaCompleta.substring(0,25) + cadenaCompleta.substring(27);
		cadenaCompleta = cadenaCompleta.substring(0,27) + cadenaCompleta.substring(29);
	}else if(Long == 11){
		Key = cadena.substring(21,23) + cadena.substring(25,27) + cadena.substring(29,31) + cadena.substring(33,35);
		Key += cadena.substring(37,38);
		Key += cadena.substring(38,39);
		Key += cadena.substring(41,42);
		cadenaCompleta = cadenaCompleta.substring(0,19) + cadenaCompleta.substring(21);
		cadenaCompleta = cadenaCompleta.substring(0,21) + cadenaCompleta.substring(23);
		cadenaCompleta = cadenaCompleta.substring(0,23) + cadenaCompleta.substring(25);
		cadenaCompleta = cadenaCompleta.substring(0,25) + cadenaCompleta.substring(27);
		cadenaCompleta = cadenaCompleta.substring(0,27) + cadenaCompleta.substring(29);
		cadenaCompleta = cadenaCompleta.substring(0,29) + cadenaCompleta.substring(30);
		
	}else if(Long == 12){
		Key = cadena.substring(21,23) + cadena.substring(25,27) + cadena.substring(29,31) + cadena.substring(33,35);
		Key += cadena.substring(37,38);
		Key += cadena.substring(38,39);
		Key += cadena.substring(41,42);
		Key += cadena.substring(42,43);
		
		cadenaCompleta = cadenaCompleta.substring(0,19) + cadenaCompleta.substring(21);
		cadenaCompleta = cadenaCompleta.substring(0,21) + cadenaCompleta.substring(23);
		cadenaCompleta = cadenaCompleta.substring(0,23) + cadenaCompleta.substring(25);
		cadenaCompleta = cadenaCompleta.substring(0,25) + cadenaCompleta.substring(27);
		cadenaCompleta = cadenaCompleta.substring(0,27) + cadenaCompleta.substring(29);
		cadenaCompleta = cadenaCompleta.substring(0,29) + cadenaCompleta.substring(31);
	}else if(Long == 13){
                Key = cadena.substring(21,23) + cadena.substring(25,27) + cadena.substring(29,31) + cadena.substring(33,35);
		Key += cadena.substring(37,38);
		Key += cadena.substring(38,39);
		Key += cadena.substring(41,42);
		Key += cadena.substring(42,43);
                Key += cadena.substring(45,46);
				
                cadenaCompleta = cadenaCompleta.substring(0,19)+ cadenaCompleta.substring(21) ;
                cadenaCompleta = cadenaCompleta.substring(0,21)+ cadenaCompleta.substring(23) ;
                cadenaCompleta = cadenaCompleta.substring(0,23)+ cadenaCompleta.substring(25) ;
                cadenaCompleta = cadenaCompleta.substring(0,25)+ cadenaCompleta.substring(27) ;
                cadenaCompleta = cadenaCompleta.substring(0,27)+ cadenaCompleta.substring(29) ;
                cadenaCompleta = cadenaCompleta.substring(0,29)+ cadenaCompleta.substring(31) ;
                cadenaCompleta = cadenaCompleta.substring(0,31)+ cadenaCompleta.substring(32) ;
        }
        
	String tempo="";
	for(int y=Long-1; y != -1; y--)
	{
		tempo += Key.substring(y, y+1) ;
		
	}

        Res[2]=Key;
	Key = parsePass(tempo);
	Res[0]=Key;
	Res[1]=cadenaCompleta;
        //logger.debug("Key: " + Key);
        //logger.debug("cadenaCompleta: " + cadenaCompleta);
	
	return Res;
    }
    
public String EnviaMailIAVE(String Nombre,  String Mail, String TipoMensaje, String Asunto, String Usuario, String Password, 
        String Monto, String Autorizacion, String Comision, String Saldo,String AutorizacionIAVE, String tag, String idBitacora){
    try{
       java.util.Date fecha = new java.util.Date();
       SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        String wAsunto = Asunto;
        String Cadena = TipoMensaje;
        
        Cadena = Cadena.replace("@NOMBRE", Nombre);
        Cadena = Cadena.replace("@USUARIO", Usuario);
        Cadena = Cadena.replace("@PASSWORD", Password);
        Cadena = Cadena.replace("@MONTO", Monto);
        Cadena = Cadena.replace("@AUTORIZACION_IAVE", AutorizacionIAVE);
        Cadena = Cadena.replace("@AUTORIZACION", Autorizacion);
        Cadena = Cadena.replace("@COMISION", Comision);
        Cadena = Cadena.replace("@FECHA", dateFormat.format(fecha));
        Cadena = Cadena.replace("@SALDO", Saldo);
        Cadena = Cadena.replace("@TAG_IAVE", tag);

        if(Cadena.indexOf("@CARGO") >= 0 ){
            double Car = Double.parseDouble(Monto) +  Double.parseDouble(Comision);
            Cadena = Cadena.replace("@CARGO", "" +Car);
        }
        
        if(Cadena.indexOf("@REFERENCIA") >= 0 ){
            Cadena = Cadena.replace("@REFERENCIA", idBitacora);
        }

        boolean x= U.sendMail(Mail, wAsunto ,Cadena);

        if(x){
            return "0";
        }else
        {
            return "-1";
        }
    }catch(Exception e){
        logger.error("EnviaMail: " + e);
        return "-1";
    }
}
    
}
