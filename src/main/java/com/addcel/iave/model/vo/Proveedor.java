/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.addcel.iave.model.vo;


/**
 *
 * @author -
 */
public class Proveedor {
     private String clave;
     private String claveWS;
     private String path;
     private String descripcion;
     private int id_categoria;

     private long idusuario;
     private long compatible;
     private long tipotarjeta;
     private long plataforma;
     

    public void setPlataforma(long plataforma) {
        this.plataforma = plataforma;
    }

    /**
     * @return the clave
     */
    public long getPlataforma() {
        return plataforma;
    }

     
              /**
     * @param clave the clave to set
     */
    public void setCompatible(long compatible) {
        this.compatible = compatible;
    }

    /**
     * @return the clave
     */
    public long getCompatible() {
        return compatible;
    }


     
         /**
     * @param clave the clave to set
     */
    public void setTipoTarjeta(long tipotarjeta) {
        this.tipotarjeta = tipotarjeta;
    }

    /**
     * @return the clave
     */
    public long getTipoTarjeta() {
        return tipotarjeta;
    }


    /**
     * @param clave the clave to set
     */
    public void setIdUsuario(long idusuario) {
        this.idusuario = idusuario;
    }

    /**
     * @return the clave
     */
    public long getIdUsuario() {
        return idusuario;
    }

             /**
     * @return the clave
     */
    public String getClave() {
        return clave;
    }

    /**
     * @param clave the clave to set
     */
    public void setClave(String clave) {
        this.clave = clave;
    }

    /**
     * @return the claveWS
     */
    public String getClaveWS() {
        return claveWS;
    }

    /**
     * @param claveWS the claveWS to set
     */
    public void setClaveWS(String claveWS) {
        this.claveWS = claveWS;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the id_categoria
     */
    public int getId_categoria() {
        return id_categoria;
    }

    /**
     * @param id_categoria the id_categoria to set
     */
    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }
}
