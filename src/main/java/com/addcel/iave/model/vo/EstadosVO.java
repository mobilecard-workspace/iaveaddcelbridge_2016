package com.addcel.iave.model.vo;

import java.util.List;

public class EstadosVO {
    private Integer idedo;

    private String nombre;
    
    private List<EstadosVO> estados;

    public Integer getIdedo() {
        return idedo;
    }

    public void setIdedo(Integer idedo) {
        this.idedo = idedo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public List<EstadosVO> getEstados() {
        return estados;
    }

    public void setEstados(List<EstadosVO> estados) {
        this.estados = estados;
    }
}