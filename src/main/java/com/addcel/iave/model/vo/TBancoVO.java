package com.addcel.iave.model.vo;

import java.util.List;

public class TBancoVO {

	private Integer id_banco;

    private String desc_banco;

    private String desc_banco_corta;

    private Integer bin_bajo;

    private Integer bin_alto;

    private Byte activo;
    
    private List<TBancoVO> bancos;

    public List<TBancoVO> getBancos() {
		return bancos;
	}

	public void setBancos(List<TBancoVO> bancos) {
		this.bancos = bancos;
	}

	public Integer getId_banco() {
        return id_banco;
    }

    public void setId_banco(Integer id_banco) {
        this.id_banco = id_banco;
    }

    public String getDesc_banco() {
        return desc_banco;
    }

    public void setDesc_banco(String desc_banco) {
        this.desc_banco = desc_banco;
    }

    public String getDesc_banco_corta() {
        return desc_banco_corta;
    }

    public void setDesc_banco_corta(String desc_banco_corta) {
        this.desc_banco_corta = desc_banco_corta;
    }

    public Integer getBin_bajo() {
        return bin_bajo;
    }

    public void setBin_bajo(Integer bin_bajo) {
        this.bin_bajo = bin_bajo;
    }

    public Integer getBin_alto() {
        return bin_alto;
    }

    public void setBin_alto(Integer bin_alto) {
        this.bin_alto = bin_alto;
    }

    public Byte getActivo() {
        return activo;
    }

    public void setActivo(Byte activo) {
        this.activo = activo;
    }
}