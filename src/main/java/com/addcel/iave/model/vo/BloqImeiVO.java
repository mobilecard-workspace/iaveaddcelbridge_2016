package com.addcel.iave.model.vo;

public class BloqImeiVO {
    private String imei;

    private Integer activo;

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }
}