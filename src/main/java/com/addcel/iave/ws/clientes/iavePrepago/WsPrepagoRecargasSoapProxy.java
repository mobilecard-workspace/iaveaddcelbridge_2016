package com.addcel.iave.ws.clientes.iavePrepago;

public class WsPrepagoRecargasSoapProxy implements com.addcel.iave.ws.clientes.iavePrepago.WsPrepagoRecargasSoap {
  private String _endpoint = null;
  private com.addcel.iave.ws.clientes.iavePrepago.WsPrepagoRecargasSoap wsPrepagoRecargasSoap = null;
  
  public WsPrepagoRecargasSoapProxy() {
    _initWsPrepagoRecargasSoapProxy();
  }
  
  public WsPrepagoRecargasSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initWsPrepagoRecargasSoapProxy();
  }
  
  private void _initWsPrepagoRecargasSoapProxy() {
    try {
      wsPrepagoRecargasSoap = (new com.addcel.iave.ws.clientes.iavePrepago.WsPrepagoRecargasLocator()).getwsPrepagoRecargasSoap();
      if (wsPrepagoRecargasSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wsPrepagoRecargasSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wsPrepagoRecargasSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wsPrepagoRecargasSoap != null)
      ((javax.xml.rpc.Stub)wsPrepagoRecargasSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.iave.ws.clientes.iavePrepago.WsPrepagoRecargasSoap getWsPrepagoRecargasSoap() {
    if (wsPrepagoRecargasSoap == null)
      _initWsPrepagoRecargasSoapProxy();
    return wsPrepagoRecargasSoap;
  }
  
  public java.lang.String reload(java.lang.String sXML) throws java.rmi.RemoteException{
    if (wsPrepagoRecargasSoap == null)
      _initWsPrepagoRecargasSoapProxy();
    return wsPrepagoRecargasSoap.reload(sXML);
  }
  
  public java.lang.String reverseReload(java.lang.String sXML) throws java.rmi.RemoteException{
    if (wsPrepagoRecargasSoap == null)
      _initWsPrepagoRecargasSoapProxy();
    return wsPrepagoRecargasSoap.reverseReload(sXML);
  }
  
  
}