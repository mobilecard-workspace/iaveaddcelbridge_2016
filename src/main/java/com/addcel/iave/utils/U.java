package com.addcel.iave.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.addcel.iave.servicios.services.IaveServiciosService;
import crypto.Crypto;
import java.util.Random;

/**
 *
 * @author marco
 * Metodos estaticos generales de uso
 */
public class U {
    private static final Logger logger = LoggerFactory.getLogger(U.class);
    
    @Autowired
    private static IaveServiciosService service;
    
    private static String HOST = null;
    private static int PORT = 0;
    private static String USERNAME = null;
    private static String PASSWORD = null;
    private static String FROM = null; 
    private static Properties PROPS = null;
    
    private static final String TelefonoServicio="5525963513";
    
    private static final String URL_HEADER_ADDCEL = "/usr/java/resources/images/Addcel/MobileCard_header.PNG";
    
    private static DecimalFormat formato;
    private static final String patronImp = "###,###,##0.00";
    private static DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
    
    static{
        logger.debug("Obteniendo datos de coneccion SMTP par envio correos.");
        try{
            PROPS = new Properties();
            PROPS.put("mail.smtp.auth", "true");
            PROPS.put("mail.smtp.starttls.enable", "true");
            /*logger.debug("HOST: " + HOST);
            logger.debug("PORT: " + PORT);
            logger.debug("USERNAME: " + USERNAME);*/
            //logger.debug("PASSWORD: " + PASSWORD);
            
            simbolos.setDecimalSeparator('.');
            formato = new DecimalFormat(patronImp,simbolos);
        } catch (Exception e) {
            logger.error("ERROR - SEND MAIL: " + e.toString());
        }
    }
        
    public  boolean esCorreo(String correo){
            return (correo.matches("^[_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@+([_a-zA-Z0-9-]+\\.)*[a-zA-Z0-9-]{2,200}\\.[a-zA-Z]{2,6}$"));
    }

   public static boolean sendMail(String destinatario,String asunto,String cuerpo){
        try 
        {
            HOST = service.consultaParametros("@SMTP");
            logger.debug("HOST : " + HOST);
            PORT = Integer.parseInt(service.consultaParametros("@SMTP_PORT") );
            logger.debug("PORT : " + PORT);
            USERNAME =  service.consultaParametros("@SMTP_USER");
            logger.debug("USERNAME : " + USERNAME);
            PASSWORD =  service.consultaParametros("@SMTP_PWD");
            logger.debug("PASSWORD : " + PASSWORD);
            FROM = service.consultaParametros("@SMTP_MAIL_SEND");
            logger.debug("FROM : " + FROM);
            PROPS = new Properties();
            PROPS.put("mail.smtp.auth", "true");
            PROPS.put("mail.smtp.starttls.enable", "true");    
            Session session = Session.getInstance(PROPS);

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress( FROM ));
//			message.setFrom(new InternetAddress( "addcel99@gmail.com" ));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario));
            message.setSubject(asunto);

            if(cuerpo.indexOf("cid:identifierCID00") > 0 ){
                //logger.debug("Se agrega imagen: " + URL_HEADER_ADDCEL);
                BodyPart messageBodyPart = new MimeBodyPart();

                // Set the HTML content, be sure it references the attachment
                //String htmlText = "<H1>Hello</H1>" +  "<img src=\"cid:memememe\">";

                // Set the content of the body part
                messageBodyPart.setContent(cuerpo, "text/html");

                // Create a related multi-part to combine the parts
                MimeMultipart multipart = new MimeMultipart("related");

                // Add body part to multipart
                multipart.addBodyPart(messageBodyPart);

                // Create part for the image
                messageBodyPart = new MimeBodyPart();

                // Fetch the image and associate to part
                DataSource fds = new FileDataSource(URL_HEADER_ADDCEL);
                messageBodyPart.setDataHandler(new DataHandler(fds));

                // Add a header to connect to the HTML
                messageBodyPart.setHeader("Content-ID","identifierCID00");

                // Add part to multi-part
                multipart.addBodyPart(messageBodyPart);

                // Associate multi-part with message
                message.setContent(multipart);
            }else{
                //logger.debug("Se agrega contenido HTML sin imagen...");
                // Create your new message part
                message.setContent(cuerpo, "text/html");
            }

            Transport transport = session.getTransport("smtp");
            transport.connect(HOST, PORT, USERNAME, PASSWORD);
            message.saveChanges();
//			Transport.send(message);
            transport.sendMessage(message,message.getAllRecipients());
            
            logger.debug("Correo enviado exitosamente: " + destinatario);

            return true;

        } catch (Exception e) {
            logger.error("ERROR - SEND MAIL: " + e);
            return false;
        }
    }
    
    public  void Display(String Cadena){
        //logger.debug("*******************************************************************************");
        logger.debug(Cadena);
        //logger.debug("*******************************************************************************");
    }
    
    public static void DisplayCadena(String Cadena){
        logger.debug(Cadena);
    }
    
    public static String formatoImporte(double numDouble){
        String total = null;
        try{
            total = formato.format(numDouble);
        }catch(Exception e){
                logger.error("Error en formatoImporte: "+ e.getMessage());
        }
        return total;
    }
    
    public String XMLTestVIAPASS(){
		String parseo="";
		
                parseo +="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                parseo +="<Response>";
                parseo +="<Resultado>1</Resultado>";
                parseo +="<CodError>511</CodError>";
                parseo +="</Response>]]>";
                
                parseo = "<![CDATA[<?xml version=\"1.0\" encoding='iso-8859-1' ?>  <Response>    <Resultado>10</Resultado>    <CodError>511</CodError>  </Response>";
                        
		return parseo;
    }    
    public String XMLTestOHL(){
		String parseo="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		parseo +="<topup_service>";
		parseo += "<request>";
		parseo += "	<user>serviceuser</user>";
		parseo += "	<password>serviceuserpwd</password>";
		parseo += "	<id_tag>09900007066</id_tag>";
		parseo += "</request>";
		parseo += "<response>";
		parseo += "	<user>service</user>";
		parseo += "	<password>servicepwd</password>";
		parseo += "	<balance>56876</balance>";
		parseo += "	<seq>999999999</seq>";
		parseo += "	<ret_code>0</ret_code>";
		parseo += "	<ret_msg></ret_msg>";
		parseo += "</response>";
		parseo += "</topup_service>";
                
		return parseo;
    }
    
    private static int AUTONO_PASE = 991000;
    
    public String XMLTestPASE(){
	boolean aut = ((Math.floor(Math.random()*(40-1+1)+1)) % 4 == 0);
	StringBuffer x = new StringBuffer()
		.append( "<?xml version=\"1.0\" encoding=\"utf-8\"?>" )
		.append( "<ReloadResponse>" )
		.append( "  <ID_GRP>99999999</ID_GRP>" )
		.append( "  <CARD_NUMBER>0000000</CARD_NUMBER>") 
		.append( "  <CHECK_DIGIT>3</CHECK_DIGIT>")
		.append( "  <LOCAL_DATE>10/10/2012 17:50:20</LOCAL_DATE>")
		.append( "  <AMOUNT>30</AMOUNT>" )
		.append( "  <TRX_NO>46</TRX_NO>" )
		.append( "  <AUTONO>" ).append( AUTONO_IAVE++ ).append("</AUTONO>" )
		.append( "  <RESPONSECODE>" ).append( aut? "00":"771" )
                .append("</RESPONSECODE>" )
		.append( "  <DESCRIPTIONCODE>" ).append( aut? "RECARGA VALIDA (SIMULACION)": "Servicio PASE no disponible(SIMULACION)" ).append("</DESCRIPTIONCODE>" )
		.append( "</ReloadResponse>" );
	return x.toString();
    }
    
    private static int AUTONO_IAVE = 910000;
    
    public String XMLTestIAVE(){
        boolean aut = ((Math.floor(Math.random()*(40-1+1)+1)) % 4 == 0);
		StringBuffer x = new StringBuffer()
			.append( "<?xml version=\"1.0\" encoding=\"utf-8\"?>" )
			.append( "<ReloadResponse>" )
			.append( "  <ID_GRP>99999999</ID_GRP>" )
			.append( "  <CARD_NUMBER>0000000</CARD_NUMBER>") 
			.append( "  <CHECK_DIGIT>3</CHECK_DIGIT>")
			.append( "  <LOCAL_DATE>10/10/2012 17:50:20</LOCAL_DATE>")
			.append( "  <AMOUNT>30</AMOUNT>" )
			.append( "  <TRX_NO>46</TRX_NO>" )
			.append( "  <AUTONO>" ).append( AUTONO_IAVE++ ).append("</AUTONO>" )
			.append( "  <RESPONSECODE>" ).append( "00" ) // aut? "00":"771" )
			.append("</RESPONSECODE>" )
			.append( "  <DESCRIPTIONCODE>" ).append( "RECARGA VALIDA (SIMULACION)" ) //aut? "RECARGA VALIDA (SIMULACION)": "Servicio IAVE no disponible(SIMULACION)" )
			.append("</DESCRIPTIONCODE>" )
			.append( "</ReloadResponse>" );
	return x.toString();
    }
    public String XMLTest(){
		
	String x="";

		x += "<ns2:executePurchaseResponse xmlns:ns2=\"http://addcel.webservice.spv.com/\">";
		x += "         <return>";
		x += "<![CDATA[<PINlisting orderID=\"999999\">";
		x += "<pin pinID=\"\" purchaseDate=\"10/10/2011 15:54:48\" voidDate=\"\">";
		x += "<card cardID=\"358\" carrier=\"TELCEL\" partNumber=\"TELCELFORWTS 20\" region=\"\" amount=\"20.00\" wholesalePrice=\"\" icon=\"\" upc=\"\"> </card>";
		x += "<ticket>PRUEBAAA  Radiocomunicaciones SOAP";
		x += "Paseo La Asuncion 266 Casa 33 Cond B";
		x += "Rancho Las Palomas";
		x += "Metepec, Metepec 52149";
		x += "";
		x += "10/10/2011 15:54:48 Ticket No:126513376";
		x += "";
		x += "   TELCEL $20.00";
		x += "";
		x += "   Numero celular :5534918527";
		x += "";
		x += "   Autorizacion Carrier :096763";
		x += "";
		x += "TELCEL Recarga Electronica";
		x += "";
		x += "Estimado cliente, en caso de ";
		x += "presentarse algun problema con ";
		x += "su tiempo aire. ";
		x += "favor de comunicarse a Atencion a";
		x += "Clientes Telcel al *264 desde";
		x += "su equipo. ";
		x += "";
		x += "Para informacion sobre tarifas y";
		x += "vigencias de acuerdo al monto abonado. ";
		x += "favor de consultar:";
		x += "           www.telcel.com.mx ";
		x += "";
		x += "Una vez que la recarga ha sido abonada ";
		x += "no habra devolucion.";
		x += "";
		x += "EL VALOR DE ESTE TICKET";
		x += "NO ES REEMBOLSABLE</ticket>";
		x += "</pin>";
		x += "<ProsaResponse>";
		x += "<track2>00000001</track2>";
		x += "<claveOperacion>0000002</claveOperacion>";
		x += "<autorizacion>000003</autorizacion>";
		x += "</ProsaResponse>";
		x += "</PINlisting>]]></return>";
		x += "      </ns2:executePurchaseResponse>";
    return x;
    }
    
    public static String parsePass(String pass){
        int len = pass.length();
        String key = "";

        for (int i =0; i < 32 /len; i++){
              key += pass;
        }

        int carry = 0;
        while (key.length() < 32){
              key += pass.charAt(carry);
              carry++;
        }
        return key;
    }
    
    public static String setSMS(String Telefono) {
        return Crypto.aesEncrypt(parsePass(TelefonoServicio), Telefono);
    }

    public static String getSMS(String Telefono) {
        return Crypto.aesDecrypt(parsePass(TelefonoServicio), Telefono);
    }

    public static String random() {
        Random rand = new Random();
        int x = rand.nextInt(99999999);
        return "" + x;
    }
}
