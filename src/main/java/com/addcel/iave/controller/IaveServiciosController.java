package com.addcel.iave.controller;

import static com.addcel.iave.utils.Constantes.LOG_PROCESO;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_CATALOGOS_SERVICIOS;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_CATALOGOS_PARAMETROS;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_INICIO_CONSULTA_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_SALDO;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_PAGO_SERVICIOS;
import static com.addcel.iave.utils.Constantes.TESTING_SERVICES_WEB;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.iave.model.vo.UsuarioVO;
import com.addcel.iave.servicios.services.*;
import com.google.gson.Gson;

import crypto.Crypto;

import java.util.Properties;

@Controller
public class IaveServiciosController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(IaveServiciosController.class);
    private static final String PATH_CONSULTA_PARAMETROS = "/consultaCatalogoParametros";
    private static final String PATH_CONSULTA_USUARIO = "/consultaUsuarioPorId";
    private static final String PATH_VALIDA_LOGIN_USUARIO = "/validaLoginUsuario";
    private static final String PATH_CONSULTA_PRODUCTO_CLAVE = "/consultaProductoClave";
    private static final String PATH_CONSULTA_PROVEEDOR_CLAVE = "/consultaProveedorClave";
    private static final String PATH_CONSULTA_COMISION_CLAVE_PROVEEDOR = "/consultaComisionClaveProveedor";
    private static final String PATH_ACTUALIZA_BITACORA_IAVE = "/actualizaBitacoraIave";
    private static final String PATH_ACTUALIZA_BITACORA = "/actualizaBitacora";
    private static final String PATH_CONSULTA_SERVICIOS = "/consultaCatalogoServiciosAntad";
    private static final String PATH_CONSULTA_ENDPOINTS = "/consultaEndPoints";
    private static final String PATH_REALIZA_PAGO = "/procesaPagoAntad";
    private static final String PATH_EJECUTACOMPRA_IAVE3DS = "adc_purchase_iave3Ds";
    private static final String PATH_INSERTA_USUARIO = "adc_userInsert";
    private static final String PATH_USUARIO_LOGIN = "/adc_userLogin";
    private static final String PATH_USUARIO_PASSWORD_UPDATE = "adc_userPasswordUpdate";
    private static final String PATH_USUARIO_UPDATE = "adc_userUpdate";
    private static final String PATH_GET_ESTADOS = "adc_getEstados";
    private static final String PATH_GETUSER_DATA = "adc_getUserData";
    private static final String PATH_GETTIPOS_TARJETAS = "adc_getCardType";
    private static final String PATH_GET_BANCOS = "adc_getBanks";
    private static final String PATH_GET_PROVIDERS = "adc_getProviders";
    private static final String PATH_GET_PRODUCTS = "adc_getProducts";
    private static final String PATH_REALIZA_CONSULTA = "/procesaConsultaSaldosAntad";
    private static final String PATH_TEST = "/test";
    private static final String VIEW_HOME = "home";
    private static final String REQ_PARAM_JSON = "json";
    private static final String REQ_PARAM_COMPRA = "compra";
    private static final String REQ_PARAM_TRANSACTION = "transactionProcomVO";
    
    private static Properties PROPS = null;
    private static String HOST = null;
    private static int PORT = 0;
    private static String USERNAME = null;
    private static String PASSWORD = null;
    private static String FROM = null; 
    
    private static Gson gson = new Gson();
    private static String key = "1234567890ABCDEF0123456789ABCDEF";
	
	public IaveServiciosController() {

	}
	
	@Autowired
	public IaveServiciosService iaveService;
	@Autowired
	public UsuarioServices usuarioService;
	
	@RequestMapping(value = PATH_TEST, method=RequestMethod.GET)
	public ModelAndView home() {	
		LOGGER.info(TESTING_SERVICES_WEB);
		ModelAndView mav = new ModelAndView(VIEW_HOME);
		return mav;	
	}
	
	@RequestMapping(value = PATH_CONSULTA_SERVICIOS, method=RequestMethod.POST)
	public @ResponseBody String consultaServicios(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_CONSULTA_CATALOGOS_SERVICIOS);
		return iaveService.consultaServicios(jsonEnc);
	}

	@RequestMapping(value = PATH_CONSULTA_PARAMETROS, method=RequestMethod.GET)
	public @ResponseBody String consultaParametros(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_CONSULTA_CATALOGOS_PARAMETROS);
		return iaveService.consultaParametros(jsonEnc);
	}
	
	@RequestMapping(value = PATH_CONSULTA_USUARIO, method=RequestMethod.POST)
	public @ResponseBody String consultaUsuarioById(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_INICIO_CONSULTA_USUARIO);
		return usuarioService.consultaUsuarioById(jsonEnc).getUsr_login();
	}
	
	@RequestMapping(value = PATH_VALIDA_LOGIN_USUARIO, method=RequestMethod.POST)
	public @ResponseBody UsuarioVO validaLoginUsuario(@RequestParam(REQ_PARAM_JSON) String jsonEnc, @RequestParam(REQ_PARAM_JSON) String pwd) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_INICIO_CONSULTA_USUARIO);
		LOGGER.info("usuario: "+jsonEnc);
		LOGGER.info("password: "+pwd);
		return iaveService.validaUsuario(jsonEnc,pwd);
	}
	
	@RequestMapping(value = PATH_CONSULTA_PRODUCTO_CLAVE, method=RequestMethod.POST)
	public @ResponseBody String consultaProducto(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_INICIO_CONSULTA_USUARIO);
		return iaveService.consultaProducto(jsonEnc).getNombre();
	}
	
	@RequestMapping(value = PATH_CONSULTA_PROVEEDOR_CLAVE, method=RequestMethod.POST)
	public @ResponseBody String consultaProveedor(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_INICIO_CONSULTA_USUARIO);
		return iaveService.consultaProveedor(jsonEnc).getPrv_nombre_completo();
	}
	
	@RequestMapping(value = PATH_CONSULTA_COMISION_CLAVE_PROVEEDOR , method=RequestMethod.POST)
	public @ResponseBody String consultaComision(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_INICIO_CONSULTA_USUARIO);
		return Double.toString(iaveService.consultaComision(jsonEnc));
	}
	
	@RequestMapping(value = PATH_CONSULTA_ENDPOINTS, method=RequestMethod.POST)
	public @ResponseBody String consultaEndPointsService(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		return iaveService.consultaEndPointsService(jsonEnc);
	}
	
	/*nuevo Request mapping con la nueva arquitectura de mybatis y Spring MVC*/
	@RequestMapping(value = PATH_EJECUTACOMPRA_IAVE3DS, method=RequestMethod.POST)
	public @ResponseBody String ejecutaCompra(@RequestParam(REQ_PARAM_JSON)  String jsonEnc, @RequestParam(REQ_PARAM_TRANSACTION) String transaction) {
	        LOGGER.info("Ejecuta Compra IAVE ");
		return iaveService.ejecutaCompraService(jsonEnc, transaction);
	}
	
        /*nuevo Request mapping con la nueva arquitectura de mybatis y Spring MVC*/
	@RequestMapping(value = PATH_INSERTA_USUARIO, method=RequestMethod.POST)
	public @ResponseBody String insertaUsuario(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
              LOGGER.info("Ejecuta insertaUsuario ");
              return usuarioService.insertaUsuarioService(jsonEnc);
         }
    
    @RequestMapping(value = PATH_USUARIO_LOGIN, method=RequestMethod.POST)
	public @ResponseBody String usuarioLogin(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
        /*LOGGER.info("Ejecuta usuarioLogin : " + jsonEnc);
        return usuarioService.usuarioLoginService(jsonEnc);*/
    	String resp = "";
        LOGGER.info("Entro a usuarioLogin: " + resp);
        resp = usuarioService.usuarioLoginService(jsonEnc);
        LOGGER.info("Respuesta usuarioLogin: " + resp);
        return resp;
    }
    
    @RequestMapping(value = PATH_USUARIO_PASSWORD_UPDATE, method=RequestMethod.POST)
	public @ResponseBody String usuarioPasswordUpdate(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
        LOGGER.info("Ejecuta usuarioPasswordUpdate ");
        return usuarioService.usuarioPasswordUpdateService(jsonEnc);
    }
    
    @RequestMapping(value = PATH_USUARIO_UPDATE, method=RequestMethod.POST)
	public @ResponseBody String usuarioUpdate(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
        LOGGER.info("Ejecuta usuarioUpdate");
        return usuarioService.usuarioUpdateService(jsonEnc);
    }
    
    @RequestMapping(value = PATH_GET_ESTADOS, method=RequestMethod.POST)
	public @ResponseBody String getEstados(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
        LOGGER.info("Ejecuta getEstados");
        return Crypto.aesEncrypt(key, gson.toJson(iaveService.getEstados()));
    }
    @RequestMapping(value = PATH_GETUSER_DATA, method=RequestMethod.POST)
	public @ResponseBody String getUserData(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
        LOGGER.info("Respuesta getUserData ");
    	return usuarioService.getUserDataService(jsonEnc);
    }
    
    @RequestMapping(value = PATH_GETTIPOS_TARJETAS, method=RequestMethod.POST)
	public @ResponseBody String getTiposTarjetas(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
        LOGGER.info("Ejecuta getTiposTarjetas");
    	return Crypto.aesEncrypt(key, gson.toJson(iaveService.getTiposTarjeta()));
    }
    
    @RequestMapping(value = PATH_GET_BANCOS, method=RequestMethod.POST)
	public @ResponseBody String getBancos(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
        LOGGER.info("Ejecuta getBancos");
    	return Crypto.aesEncrypt(key, gson.toJson(iaveService.getBancos()));
    }
    
    @RequestMapping(value = PATH_GET_PROVIDERS, method=RequestMethod.POST)
	public @ResponseBody String getProveedores() {
   	LOGGER.info("Ejecuta getProveedores");
     	return Crypto.aesEncrypt(key, gson.toJson(iaveService.getProveedores()));
    }
    
    @RequestMapping(value = PATH_GET_PRODUCTS, method=RequestMethod.POST)
	public @ResponseBody String getProductos(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
    	String resp = usuarioService.getProductosService(jsonEnc);
    	return resp;
    }
    
}
