/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.iave.servicios.services;

import com.addcel.iave.model.mapper.IaveServiciosMapper;
import com.addcel.iave.model.vo.OzekimessageoutVO;
import com.addcel.iave.model.vo.Producto;
import com.addcel.iave.model.vo.ProveedorVO;
import com.addcel.iave.model.vo.Usuario;
import com.addcel.iave.model.vo.UsuarioTagsVO;
import com.addcel.iave.model.vo.UsuarioVO;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_PROVEEDOR;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_PROVEEDOR;
import static com.addcel.iave.utils.Constantes.LOG_SERVICE;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author JCDP
 */
@Service
public class UtilsServiciosService {
    
    @Autowired
    private IaveServiciosMapper mapper;
    
    @Autowired
    public UsuarioServices usuarioService;
    
    private static final Logger LOGGER = LoggerFactory.getLogger(UtilsServiciosService.class);
    
    public boolean ExisteMail(Usuario usuario) {
        boolean flag = false;
        UsuarioVO usuaux = new UsuarioVO();
        try {
            usuaux = usuarioService.consultaUsuarioByEmail(usuario.getMail());
            if (usuaux != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error ExisteMail {}",ex);
        } finally {
            LOGGER.error("Validacion de e-mail de usuario {}");
        }
        return flag;
    }    
    
    public boolean ExisteT(Usuario usuario) {
        boolean flag = false;
        UsuarioVO usuaux = new UsuarioVO();
        try {
            usuaux = usuarioService.consultaUsuarioByT(usuario.getTarjeta());
            if (usuaux != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error ExisteT {}",ex);
        } finally {
            LOGGER.error("Validacion de tdc de usuario {}");
        }
        return flag;
    }    
    
    public boolean ExisteIMEI(Usuario usuario) {
        boolean flag = false;
        UsuarioVO usuaux = new UsuarioVO();
        try {
            usuaux = usuarioService.consultaUsuarioByImei(usuario.getImei());
            if (usuaux != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error ExisteIMEI {}",ex);
        } finally {
            LOGGER.error("Validacion de imei de usuario {}");
        }
        return flag;
    }    
    
    public boolean ExisteTelefono(Usuario usuario) {
        boolean flag = false;
        UsuarioVO usuaux = new UsuarioVO();
        try {
            usuaux = usuarioService.consultaUsuarioByTelefono(usuario.getTelefono());
            if (usuaux != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error ExisteTelefono {}",ex);
        } finally {
            LOGGER.error("Validacion de telefono de usuario {}");
        }
        return flag;
    }    
    
    /*               INICIO DEL CAMBIO
     * SE AGREGO ESTE METODO PARA EL ENVIO DE MENSAJES DE TEXTO
     * FUNCIONA CON LA APLICACION DE ADDCELBATCH
     * TODOS LOS REGISTROS QUE LLEGUEN A LA TABLA ozekimessageout
     * SE ESTAN VERIFICANDO EN BUSQUEDA DEL STATUS "Send" SI TIENE
     * ESTE ESTADO EL ADDCELBATCH LO ENCUENTRA Y LO ENVIA CAMBIANDO EL 
     * STATUS A "transmitted Y REGISTRANDO SU SALIDA EN smsOut
     */
    public boolean EnviarSMS(String numero, String mensaje){
        boolean flag = false;
        OzekimessageoutVO oze = new OzekimessageoutVO();
        try {
            oze.setReceiver(numero);
            oze.setMsg(mensaje);
            oze.setOperator("MobileCard");
            oze.setMsgtype("SMS:TEXT:GSM7BIT");
            oze.setSender("MobileCard");
            oze.setReference("MobileCard - APP");
            oze.setStatus("Send");
            mapper.insertOzekimessageout(oze);
            flag = true;
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error EnviarSMS: {}",ex);
        } 
        return flag;
    }
    
    public boolean addTag(UsuarioTagsVO usuariotag) {
        boolean flag = false;
        try {
            mapper.insertUsuarioTags(usuariotag);
            flag = true;
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error: {}",ex);
        } 
        LOGGER.debug("Respuesta addTag: " + flag);
        return flag;
    }   
    
    /*
	 * metodo para que valida si existe un usuario con un mail y un usr_login existente 
	 * by JCDP
	 * Fecha : 19/07/2016
	 * */
	public boolean ExisteMailUp(String eMail, String idusuario){
        boolean flag = false;
        UsuarioVO user = new UsuarioVO();
        try {
            user = mapper.existeMailUp(eMail, idusuario);
            if ( user != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error en metodo ExisteMailUp clase UsuarioDAO: {}",ex);
        }
        return flag;
    }    
        
        /*
	 * metodo para que valida si existe un usuario con un numero de tarjeta y un usr_login existente 
	 * by JCDP
	 * Fecha : 19/07/2016
	 * */
	public boolean ExisteTUp(String T, String idusuario){
        boolean flag = false;
        UsuarioVO user = new UsuarioVO();
        try {
        	user = mapper.existeTUp(T, idusuario);
            if (user != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error: {}",ex);
        } 
        return flag;
    }
   
        /*****************************************/
	/*nuevo metodo que consulta proveedor por claveWS */
	/*****************************************/
	public ProveedorVO consultaProveedorByClaveWS(String claveWS) {
		ProveedorVO proveedor = new ProveedorVO();
		String  parametro = "";
		//ServiciosAntadRequest request = null;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_PROVEEDOR + claveWS);
			proveedor = mapper.getProveedorByClaveWS(claveWS);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_PROVEEDOR+proveedor.getPrv_nombre_comercio());
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return proveedor;
	}     
        
        /*****************************************/
	/*nuevo metodo que consulta proveedor por idproveedor */
	/*****************************************/
	public List<Producto> getProductosByProveedor(long idprov) {
		List<Producto> res = null;
		try {
			res = mapper.getProductosByProveedor(idprov);
			Iterator iter = res.iterator();
			while (iter.hasNext()) {
			  LOGGER.info("recorremos la lista");		
			  System.out.println(iter.next());
			}
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+"["+e.getCause()+"]");
		} 
		return res;
	}
}
