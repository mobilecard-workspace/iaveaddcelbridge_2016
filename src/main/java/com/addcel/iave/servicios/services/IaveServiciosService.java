package com.addcel.iave.servicios.services;

import com.addcel.iave.model.vo.RespuestaAmex;
import com.addcel.iave.model.vo.CompraIAVE;
import com.addcel.iave.model.vo.CompraResponce;
import static com.addcel.iave.utils.Constantes.FORMATO_FECHA_ENCRIPT;
import static com.addcel.iave.utils.Constantes.JSON_ERROR_INESPERADO;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTA_SERVICIOS;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_SELECCIONAR_REGLAS;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTAESTADOS;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_CARRIES;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTATIPOSTARJETAS;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_BANCOS;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTA_PARAMETROS;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTA_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTA_CARRIES;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_ACTUALIZA_BITACORA;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTA_COMISION;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTA_BANCOS;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTAFOLIO_IAVE;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTATIPOSTARJETAS;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_VALIDA_T;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTAESTADOS;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTA_PROVEEDOR;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_INSERTA_BITACORAIAVE;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_SELECCIONAR_REGLAS;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_ACTUALIZAR_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTAFOLIO_BANCO_IAVE;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_DESC_ERROR;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_ACTUALIZA_BITACORAIAVE;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_INSERTA_BITACORA_PROSA;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_BANCOS;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_CARRIES;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_SERVICIOS;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_PARAMETROS;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_ACTUALIZA_BITACORA;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_COMISION;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_VALIDA_T;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_PROVEEDOR;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTAFOLIO_IAVE;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_ACTUALIZA_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTAFOLIO_BANCO_IAVE;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_DESC_ERROR;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_ACTUALIZA_BITACORAIAVE;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_INSERTA_BITACORA_PROSA;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_SERVICIOS;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_PARAMETROS;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_ACTUALIZA_BITACORA;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_COMISION;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_REGLAS;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_VALIDA_T;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_TIPOSTARJETAS;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_ACTUALIZA_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_BITACORAIAVE;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_FOLIO_IAVE;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_FOLIO_BANCO_IAVE;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_ESTADOS;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_DESC_ERROR;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_ACTUALIZACION_BITACORAIAVE;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_INSERTA_BITACORA_PROSA;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_PROVEEDOR;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_INSERTA_BITACORAIAVE;
import static com.addcel.iave.utils.Constantes.LOG_SERVICE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.iave.model.mapper.IaveServiciosMapper;
import com.addcel.iave.model.vo.BitacoraiaveVO;
import com.addcel.iave.model.vo.BitacoraprosaVO;
import com.addcel.iave.model.vo.BloqImeiVO;
import com.addcel.iave.model.vo.EndPointsRequest;
import com.addcel.iave.model.vo.EndPointsResponse;
import com.addcel.iave.model.vo.ServiciosAntadRequest;
import com.addcel.iave.model.vo.ServiciosAntadResponse;
import com.addcel.iave.model.vo.TBancoVO;
import com.addcel.iave.model.vo.TBloVO;
import com.addcel.iave.model.vo.TTipoTarjetaVO;
import com.addcel.iave.utils.UtilsService;
import com.addcel.iave.model.vo.UsuarioVO;
import com.addcel.iave.model.vo.ProductoVO;
import com.addcel.iave.model.vo.ProveedorVO;
import com.addcel.iave.model.vo.BitacoraVO;
import com.addcel.iave.model.vo.EstadosVO;
import com.addcel.utils.AddcelCrypto;
import com.addcel.iave.model.vo.Proveedor;
import com.addcel.iave.model.vo.RespuestaIAVE;
import com.addcel.iave.business.amex.PagoAmex;
import com.addcel.iave.business.amex.PagoAmexImpl;
import com.addcel.iave.integration.ParametroDAO;
import com.addcel.iave.model.vo.TransactionProcomVO;
import com.addcel.iave.model.vo.ProsaResponse;
import com.addcel.iave.utils.U;
import com.addcel.iave.ws.clientes.iavePrepago.WsPrepagoRecargasSoapProxy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import crypto.Crypto;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.math.BigDecimal;

import java.util.Calendar;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@Service
public class IaveServiciosService {

	private static final Logger LOGGER = LoggerFactory.getLogger(IaveServiciosService.class);
        
	private static final SimpleDateFormat SDF = new SimpleDateFormat(FORMATO_FECHA_ENCRIPT);
	
        @Autowired
	public UsuarioServices usuarioService;
        
	@Autowired
	private IaveServiciosMapper mapper;
	
	@Autowired
	private UtilsService jsonUtils;
	
    /*@Autowired
    @Qualifier("webServiceAntadBridge")
    private transient WebServiceTemplate webServiceTemplate;*/
	
        /*******************************************nuevos servicios para de acuerdo a la nueva arquitectura  *********************************/
        
        public String ejecutaCompraService(String jsonEnc, String transaction){
            String res = "";
		LOGGER.info("Inicio RECARGA IAVE... " );
		String json = null;
		String key = "1234567890ABCDEF0123456789ABCDEF";
		String respuesta = null;

		CompraIAVE compraiave  = null;
		CompraResponce compraResponce = null;
		TransactionProcomVO transactionProcomVO = null;
		//LOGGER.info("jsonEnc contiene : " + jsonEnc );
		
		//LOGGER.info("Despues de Inicio RECARGA IAVE" );
		if(jsonEnc == null ) {
			LOGGER.info("parameter json null " );
		}
		if(jsonEnc.equals("")){
		    json = (String) jsonEnc.toString();
		    //LOGGER.info("Despues de Inicio RECARGA IAVE Dentro de If" );
		}else{
		    json = (String) jsonEnc.toString();
		    //LOGGER.info("Despues de Inicio RECARGA IAVE Dentro de else" );
		}
		LOGGER.info("CADENA PurchaseIave: "  + json);
		Gson gson = new Gson();
		try{
		    String [] Cad = new com.addcel.iave.integration.ParametroDAO().Asf(json);
		    key = Cad[0];
		    json = Cad[1];
		    Type collectionType = new TypeToken<CompraIAVE>(){}.getType();
		    compraiave = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);
		}catch(Exception e){	
		    compraResponce = new CompraResponce();
		    compraResponce.setMensaje("password incorrecto");
		    e.printStackTrace();
		}
		transactionProcomVO = gson.fromJson(transaction,TransactionProcomVO.class);
		if((compraiave != null)&&(transactionProcomVO != null)){
			//LOGGER.info("CADENA idBitacora: "  + transactionProcomVO.getEmOrderID());
			//LOGGER.info("key : " + key );
			//LOGGER.info("Password : " + compraiave.getPassword() );
			/*INICIO DE NUEVO PROCESO CON LA NUEVA ARQUITECTURA MYBATIS SPRING MVC */
			
			UsuarioVO usuario = new UsuarioVO();
			//usuario.setPassword(compra.getPassword());
			usuario.setUsr_pwd(compraiave.getPassword());
			//usuario.setLogin(compra.getLogin());
			usuario.setUsr_login(compraiave.getLogin());
			
			double MontoCentavos = 0.0D;
			String folio = "";

			String CodeError = "";
			String DescError = "";
            
			ProsaResponse Resbanco = new ProsaResponse();
			Integer idBitacora = Integer.parseInt(transactionProcomVO.getEmOrderID());
			//LOGGER.info("Dentro de ejecutaCompraIAVE3Ds");
			//LOGGER.info("transactionProcomVO: "+transactionProcomVO);
			//LOGGER.info("getIdAplicacion: "+ compraiave.getIdAplicacion());
			
			if ((compraiave.getIdAplicacion() > 0) || validaUsuario(usuario.getUsr_login(),Crypto.aesEncrypt(key, usuario.getUsr_pwd())) != null ) { 
				try
				{
				//LOGGER.info("/*************************** ENTRO AL IF CORRECTAMENTE !!!!!! ********************************/");
				//System.out.println("/*************************** ENTRO AL IF CORRECTAMENTE !!!!!! ********************************/");
				String ActivaTrans = consultaParametros("@ACTIVA_TRANS_IAVE");
				//LOGGER.info("ActivaTrans : "+ActivaTrans);
				String Comision = consultaParametros("@MONTO_COMISION");
				//LOGGER.info("Comision : "+Comision);
				String idcom = null;
				String idgrp = null;
				String MontoCobrar = "";
				String response = null;
				if (compraiave.getIdAplicacion() > 0) {
					usuario = new UsuarioVO();
					usuario.setId_usuario(compraiave.getIdUsuario());
					usuario.setUsr_nombre(compraiave.getNombre());
					usuario.setUsr_apellido(compraiave.getApellido());
					usuario.setUsr_materno(compraiave.getMaterno());
					usuario.seteMail(compraiave.getEmail());
					usuario.setId_tipo_tarjeta(compraiave.getTipoTarjeta());
					usuario.setUsr_tdc_numero(compraiave.getTarjetaBanco());
					usuario.setUsr_tdc_vigencia(compraiave.getVigencia());
					usuario.setId_usr_status(1);
				} else {
					usuario = usuarioService.obtenerUsuario(usuario);
				}
				LOGGER.debug(
						"Inicia IAVE validacion Reglas negocio, idUsuario: " + usuario.getId_usuario() + ",  Tarjeta: "
								+ U.setSMS(usuario.getUsr_tdc_numero()) + ",  Compra TAG: " + compraiave.getTarejeta());

				compraResponce = validaReglasNegocio(usuario.getId_usuario(), U.setSMS(usuario.getUsr_tdc_numero()),
						0);
				
				if (compraResponce != null) {
					LOGGER.debug("No se satisfacen las Reglas Negocio: "+ compraResponce);
					//return compraResponce;
				}else{
					LOGGER.debug("Se satisfacen las Reglas Negocio");
				}
				compraResponce = new CompraResponce();
				compraResponce.setMensaje("password incorrecto");
				BitacoraVO bitacora = new BitacoraVO();

				bitacora.setTipo(compraiave.getTipo());
				bitacora.setSoftware(compraiave.getSoftware());
				bitacora.setModelo(compraiave.getModelo());
				bitacora.setWkey(compraiave.getKey());
				bitacora.setId_usuario(usuario.getId_usuario());
				
				//LOGGER.debug("sets bitacora" + bitacora);
				
				ProductoVO producto =consultaProducto(compraiave.getProducto());
				
				//LOGGER.debug("producto" + producto.getPro_clave());
				
				if (!esTuTag(compraiave.getProducto())) {
					idcom = consultaParametros("@ID_COM_IAVE");
					idgrp = consultaParametros("@ID_GRP_IAVE");
				} else {
					idcom = consultaParametros("@ID_COM_IAVE_TUTAG");
					idgrp = consultaParametros("@ID_GRP_IAVE_TUTAG");
				}
				double Res = consultaComision(producto.getId_proveedor().toString());
                MontoCobrar = Double.toString(Double.parseDouble(producto.getPro_monto()) + Res);
                Comision = "" + Res;
                bitacora.setId_producto(Long.parseLong(producto.getPro_clave()));
				bitacora.setBit_card_id(Integer.parseInt(producto.getPro_clave()));
				//logger.debug("MPX PROVEEDOR: " + producto.getProveedor());
				LOGGER.debug("MPX PROVEEDOR: " + producto.getId_proveedor());
				//JCDP
				LOGGER.debug("MPX COMISION: " + Comision);
				//JCDP
				//logger.debug("MPX MONTO: " + producto.getMonto());
				LOGGER.debug("MPX MONTO: " + producto.getPro_monto());
				//JCDP
				LOGGER.debug("MPX MONTOCOBRAR: " + MontoCobrar);
				BigDecimal b = new BigDecimal(producto.getPro_monto());
				bitacora.setBit_cargo(b);
				bitacora.setId_proveedor(Integer.parseInt(Long.toString(producto.getId_proveedor())));
				ProveedorVO proveedor = consultaProveedor(Long.toString(bitacora.getId_proveedor()));
				bitacora.setBit_concepto("Compra IAVE " + proveedor.getPrv_nombre_comercio() + " " + compraiave.getTarejeta() + " "
						+ producto.getPro_monto() + " Comision: " + Comision);
				bitacora.setImei(compraiave.getImei());
				bitacora.setDestino(compraiave.getTarejeta());
				bitacora.setTarjeta_compra(usuario.getUsr_tdc_numero());
				bitacora.setId_bitacora(idBitacora);
				mapper.actualizaBitacora(bitacora);
				if (compraiave.getTarejeta().equals("")) {
					bitacora.setBit_codigo_error(820);
					mapper.actualizaBitacora(bitacora);
					compraResponce.setResultado(0);
					compraResponce.setFolio("820");
					compraResponce.setMensaje("Digite o seleccione el TAG IAVE para poder realizar la recarga.");
                                        respuesta = gson.toJson(compraResponce);
					json = Crypto.aesEncrypt(key, respuesta);
					return json;
				}
				if (compraiave.getTarejeta().length() <= 4) {
					bitacora.setBit_codigo_error(820);
					mapper.actualizaBitacora(bitacora);
					compraResponce.setResultado(0);
					compraResponce.setFolio("820");
					compraResponce.setMensaje("Digite o seleccione el TAG IAVE para poder realizar la recarga.");
                                        respuesta = gson.toJson(compraResponce);
					json = Crypto.aesEncrypt(key, respuesta);
					return json;
				}
				if (compraiave.getIdAplicacion() == 0) {
					if (validaT(ParametroDAO.setSMS(usuario.getUsr_tdc_numero()))) {
						bitacora.setBit_codigo_error(999);
						actualizaBitacora(bitacora);
						actualizaUsuario(usuario);
						BuscaIm(compraiave.getImei());
						compraResponce.setResultado(0);
						compraResponce.setFolio("999");
						compraResponce.setMensaje(consultaParametros("@MENSAJE_999"));
                                                respuesta = gson.toJson(compraResponce);
                                                json = Crypto.aesEncrypt(key, respuesta);
						return json;
					}
					if (ValidaIM(compraiave.getImei())) {
						bitacora.setBit_codigo_error(998);
						mapper.actualizaBitacora(bitacora);
                        UsuarioVO usu = new UsuarioVO();
                        usu = usuarioService.consultaUsuarioById(usuario.getUsr_login());
                        usu.setId_usr_status(0);
                        actualizaUsuario(usu);
                        BTar(usuario.getUsr_tdc_numero());
						compraResponce.setResultado(0);
						compraResponce.setFolio("998");
						compraResponce.setMensaje(consultaParametros("@MENSAJE_998"));
						//LOGGER.info("antes del return ");
                                                respuesta = gson.toJson(compraResponce);
                                                json = Crypto.aesEncrypt(key, respuesta);
						return json;
					}
				}
				//LOGGER.info("ya no llego aqui");
				if (usuario.getId_usr_status() == 0) {
					bitacora.setBit_codigo_error(997);
					mapper.actualizaBitacora(bitacora);
					compraResponce.setResultado(0);
					compraResponce.setFolio("997");
					compraResponce.setMensaje(consultaParametros("@MENSAJE_997"));
                                        respuesta = gson.toJson(compraResponce);
                                        json = Crypto.aesEncrypt(key, respuesta);
					return json;
				}
				MontoCentavos = Double.parseDouble(producto.getPro_monto());
				folio = mapper.getFolioIAVE();
				
				String secuence = mapper.getFolioBancoIAVE();
				String Cobro = "" + (Double.parseDouble(producto.getPro_monto()) + Double.parseDouble(Comision));
				//rellenar datos para bitacoraProsa
				//LOGGER.info("Tipo tarjeta : " + compraiave.getTipoTarjeta());
				if (compraiave.getTipoTarjeta() != 3) { //para visa y master card		
					
					
					Resbanco.setAutorizacion(transactionProcomVO.getEmAuth());
					Resbanco.setBancoAdquirente("HSBC");
					Resbanco.setBancoEmisor("OTROS");
					Resbanco.setClaveOperacion("0");										
					Resbanco.setDireccionComercio("PASEO DE LOS TAMARINDOS 400 TORRE A P26");					
					Resbanco.setFechaHora(transactionProcomVO.getFecha());
					Resbanco.setImporte(transactionProcomVO.getEmTotal());
					
					if(compraiave.getTipoTarjeta() == 1){ 
						Resbanco.setMarca("VISA");
					}	
					else
						if(compraiave.getTipoTarjeta() == 2){
							Resbanco.setMarca("MASTER CARD");
						}
					
					Resbanco.setMoneda(transactionProcomVO.getMoneda());
					Resbanco.setNombreComercio("I+D MEXICO");
					Resbanco.setNumeroCaja(transactionProcomVO.getEmTerm());
					Resbanco.setProducto(transactionProcomVO.getProducto());
					Resbanco.setTransactionId(transactionProcomVO.getEmOrderID());
					
				} else { //si es amex
					/*
					* *******************************************************************************
					* */
					PagoAmex amex = new PagoAmexImpl();
					
					RespuestaAmex respAmex = new RespuestaAmex();
					respAmex.setTransaction(idBitacora.toString());
					respAmex.setCode("000");
					respAmex.setDsc("Approved");
					
					amex.realizarPagoTelepeaje(usuario.getUsr_tdc_numero(), usuario.getUsr_tdc_vigencia() ,
							Cobro, compraiave.getCvv2().replace("/", " ").trim(), usuario.getUsr_direccion(), usuario.getUsr_cp(),
							"IAVE");
					
					Resbanco.setAutorizacion(respAmex.getCode());
					Resbanco.setTransactionId(respAmex.getTransaction());
					Resbanco.setDescripcionRechazo(respAmex.getDsc());

					Resbanco.setBancoAdquirente("American Express");
					Resbanco.setBancoEmisor("American Express");
					Resbanco.setClaveOperacion("0");
					Resbanco.setDireccionComercio("AV P DE LA REFORMA NO 2654 P8");
					Resbanco.setError(respAmex.getDsc());
					Resbanco.setFechaHora(new Date().toString());
					Resbanco.setImporte(Cobro);
					Resbanco.setMarca("AMEX");
					Resbanco.setMoneda("MXN");
					Resbanco.setNombreComercio("ADDCEL");
					Resbanco.setNumeroCaja("99");
					Resbanco.setProducto("CREDITO");
					Resbanco.setIsAuthorized(false);
					if (respAmex.getCode().equals("000")) {
						Resbanco.setIsAuthorized(true);
					}
					/*
					* *******************************************************************************
					* */
				}
				LOGGER.debug("CADENA IAVE DESPUES BANCO IsAuthorized : " + Resbanco.isIsAuthorized());
				LOGGER.debug("CADENA IAVE DESPUES BANCO Error : " + Resbanco.getError());
				LOGGER.debug("CADENA IAVE DESPUES BANCO ClaveRechazo : " + Resbanco.getClaveRechazo());
				LOGGER.debug("CADENA IAVE DESPUES BANCO DescripcionRechazo : " + Resbanco.getDescripcionRechazo());
				
				/*Nuevo insert para nueva arquitectura con mybatis */
				BitacoraiaveVO bitvo = new BitacoraiaveVO();
				bitvo.setId_bitacora(bitacora.getId_bitacora());
				bitvo.setId_usuario(bitacora.getId_usuario());
				bitvo.setPosname(consultaParametros("@POSNAME"));
				bitvo.setPospwd(consultaParametros("@POSPWD"));
				bitvo.setUserpwd(consultaParametros("@USERPWD"));
				bitvo.setAfiliacion(Resbanco.getAfiliacion());
				bitvo.setAutorizacion(Resbanco.getAutorizacion());
				bitvo.setBancoAdquirente(Resbanco.getBancoAdquirente());
				bitvo.setBancoEmisor(Resbanco.getBancoEmisor());
				bitvo.setClaveOperacion(Resbanco.getClaveOperacion());
				bitvo.setClaveVenta("");
				bitvo.setDescripcion("");
				bitvo.setDireccionComercio(Resbanco.getDireccionComercio());
				bitvo.setErrCode("");
				bitvo.setFechaHora(Resbanco.getFechaHora());
				bitvo.setImporte(Resbanco.getImporte());
				bitvo.setMarca(Resbanco.getMarca());
				bitvo.setMoneda(Resbanco.getMoneda());
				bitvo.setNombreComercio(Resbanco.getNombreComercio());
				bitvo.setNumeroCaja(Resbanco.getNumeroCaja());
				bitvo.setProducto(Resbanco.getProducto());
				bitvo.setTrack2("");
				bitvo.setSequence(secuence);
				bitvo.setComision(Comision);
				bitvo.setCx(compraiave.getCx());
				bitvo.setCy(compraiave.getCy());
				bitvo.setImei(compraiave.getImei());
				bitvo.setTag(compraiave.getTarejeta());
				bitvo.setCliente("");
				bitvo.setTransactionid(Resbanco.getTransactionId());
				mapper.insertaBitacoraIave(bitvo);
				LOGGER.info("folio, compra.getTarejeta(), PasoMonto, compra.getPin(),idcom, idgrp, ActivaTrans");
				LOGGER.info(folio+","+compraiave.getTarejeta()+","+"" + MontoCentavos+","+compraiave.getPin()+","+idcom+","+idgrp+","+ActivaTrans);
				
				LOGGER.info("transactionProcomVO : " + transactionProcomVO.getEmResponse());
				
				if (transactionProcomVO.getEmResponse().equalsIgnoreCase("approved")) {
					String PasoMonto = "" + MontoCentavos;

					response = "";

					response = executePurchaseIAVERecarga(folio, compraiave.getTarejeta(), PasoMonto, compraiave.getPin(),
							idcom, idgrp, ActivaTrans);
					LOGGER.debug("XML IAVE RESPUESTA INTENTO 1: " + response);
					RespuestaIAVE tmp = null;

					tmp = ParceXMLIAVE(response);
					if (!tmp.getResponsecode().equals("00")) {
						folio = mapper.getFolioIAVE();

						response = executePurchaseIAVERecarga(folio, compraiave.getTarejeta(), PasoMonto, compraiave.getPin(),
								idcom, idgrp, ActivaTrans);
						LOGGER.debug("XML IAVE RESPUESTA INTENTO 2: " + response);
						tmp = ParceXMLIAVE(response);
						if (!tmp.getResponsecode().equals("00")) {
							folio = mapper.getFolioIAVE();

							response = executePurchaseIAVERecarga(folio, compraiave.getTarejeta(), PasoMonto,
									compraiave.getPin(), idcom, idgrp, ActivaTrans);
							LOGGER.debug("XML IAVE RESPUESTA INTENTO 3: " + response);
							tmp = ParceXMLIAVE(response);
							//if ((!tmp.getResponsecode().equals("00")) && (usuario.getTipotarjeta() != 3)) {
							if ((!tmp.getResponsecode().equals("00")) && (usuario.getId_tipo_tarjeta() != 3)) { 
								tmp.setResponsecode("771");
								tmp.setDescriptioncode("Error - Banco(8)");
								LOGGER.debug("ENTRO A LA REVERSA...");
								
								/********************/
								response = "ERRORREVERSE";
								CodeError = "1200";
								DescError = "No se pudo abonar saldo, solicitar devoluciÃ³n con I + D";
								/********************/
								
								/*new BitacoraDAO().addBitacoraReversa(bitacora.getId(), bitacora.getUsuario(), Resbanco,
										secuence, "IAVE");

								ProsaResponse ResReversa = null;
								if (ActivaTrans.equals("1")) {
									logger.debug("Ambiente PROD, Ejecuta reversa ");
									if (usuario.getTipotarjeta() == 3) {
										ResReversa = this.amexDAO.marcarReverso(Resbanco);
									} else {
										ResReversa = devolution(Resbanco.getTransactionId());
									}
								} else {
									logger.debug("Ambiente QA, REVERSA BITACORA OK ");
									ResReversa = new ProsaResponse();
									ResReversa.setIsAuthorized(true);
									ResReversa.setAfiliacion("7255872");
									ResReversa.setAutorizacion("" + numFolioTAE++);
									ResReversa.setBancoAdquirente("HSBC");
									ResReversa.setBancoEmisor("ORO");
									ResReversa.setClaveOperacion("4067056");

									ResReversa.setDireccionComercio(";AV P DE LA REFORMA NO 2654 P8");

									ResReversa.setFechaHora("2012-01-02T16:26:26.962-06:00");
									ResReversa.setImporte(""
											+ (Double.parseDouble(producto.getMonto()) + Double.parseDouble(Comision)));
									ResReversa.setMarca("VISA");
									ResReversa.setMoneda("MXN");
									ResReversa.setNombreComercio("ADDCEL 2");
									ResReversa.setNumeroCaja("99");
									ResReversa.setProducto("CREDITO");
								}
								if (ResReversa.isIsAuthorized()) {
									StringBuffer pXML2 = new StringBuffer().append("<tr><td> AUTORIZACION: </td><td>")
											.append(ResReversa.getAutorizacion()).append("</td></tr>")
											.append("<tr><td> IMPORTE: </td><td>$ ").append(ResReversa.getImporte())
											.append("0 MXN</td></tr>").append("<tr><td> REFERENCIA: </td><td>")
											.append(bitacora.getId()).append("</td></tr>");

									logger.debug("XML IAVE REVERSA EXITOSA: " + pXML2);

									new BitacoraDAO().UpdateBitacoraReversa(bitacora.getId(), bitacora.getUsuario(),
											pXML2 + " ");

									parametroDAO.EnviaMail(
											usuario.getNombre() + (usuario.getApellido() != null
													? " " + usuario.getApellido() : ""),
											usuario.getMail(), "@MENSAJE_REVERSO", "@ASUNTO_REVERSO_OK",
											pXML2.toString(), "", "", "", "", "");
									if (ActivaTrans.equals("1")) {
										parametroDAO.EnviaMail("Jorge", "jorge@addcel.com", "@MENSAJE_REVERSO",
												"@ASUNTO_REVERSO_OK", pXML2.toString(), "", "", "", "", "");
									}
								} else {
									String pXML2 = "";

									pXML2 = pXML2 + "[ Producto =  IAVE  ] </br>";
									pXML2 = pXML2 + "[ Id Bitacora = " + bitacora.getId() + " ] </br>";
									pXML2 = pXML2 + "[ Id ORDER/FOLIO = " + folio + " ] </br>";
									pXML2 = pXML2 + "[ Id TAG = " + compra.getTarejeta() + " ] </br>";
									pXML2 = pXML2 + "[ MONTO = " + PasoMonto + " ] </br>";

									pXML2 = pXML2 + "*** DATOS BANCARIOS ***  </br>";
									pXML2 = pXML2 + "[ AUTORIZACION = " + Resbanco.getAutorizacion() + " ] </br>";
									pXML2 = pXML2 + "[ BANCO ADQUIRIENTE = " + Resbanco.getBancoAdquirente()
											+ " ] </br>";
									pXML2 = pXML2 + "[ BANCO EMISOR = " + Resbanco.getBancoEmisor() + " ] </br>";
									pXML2 = pXML2 + "[ CLAVE OPERACION = " + Resbanco.getClaveOperacion() + " ] </br>";

									pXML2 = pXML2 + "[ FECHA - HORA = " + Resbanco.getFechaHora() + " ] </br>";
									pXML2 = pXML2 + "[ IMPORTE = " + Resbanco.getImporte() + " ] </br>";
									pXML2 = pXML2 + "[ MARCA = " + Resbanco.getMarca() + " ] </br>";
									pXML2 = pXML2 + "[ NUMERO CAJA = " + Resbanco.getNumeroCaja() + " ] </br>";
									pXML2 = pXML2 + "[ PRODUCTO = " + Resbanco.getProducto() + " ] </br>";

									pXML2 = pXML2 + "[ CODIGO ERROR DEVOLUCIO = " + ResReversa.getError() + " ] </br>";
									pXML2 = pXML2 + "[ ERROR DEVOLUCIO = " + ResReversa.getDescripcionRechazo()
											+ " ] </br>";

									logger.debug("XML IAVE REVERSA ERROR: " + pXML2);

									new BitacoraDAO().UpdateBitacoraReversa(bitacora.getId(), bitacora.getUsuario(),
											"ERROR EN WS DE REVERSA: " + pXML2 + " ");
									if (ActivaTrans.equals("1")) {
										parametroDAO.EnviaMail("Jorge", "jorge@addcel.com", "@MENSAJE_REVERSO_ERROR",
												"@ASUNTO_REVERSO_ERROR", pXML2, "", "", "", "", "");
									}
								}*/
							}
						}
					}
					if(tmp != null && tmp.getResponsecode().equals("00")){
						try{
							
							Date d = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(tmp.getLocal_date());
							/****************   Modificar metodo al final del proceso    04/07/2016
							new BitacoraDAO().updateBit_Hora(bitacora.getId(), d);
							********************************/
							bitacora.setBit_hora(d);
							mapper.actualizaBitacora(bitacora);
							LOGGER.debug("bit_hora actualizada en t_bitacora id: " + bitacora.getId_bitacora());
								
						}catch(Exception ex){
							LOGGER.error("Error al actualizar bit_hora", ex);
						}
					}
				}//fin de if transaction procomVO 	
				else {
					response = "ERROR";
					try {
						CodeError = Resbanco.getClaveRechazo();
						DescError = Resbanco.getDescripcionRechazo();
					} catch (Exception ex) {
						LOGGER.error("Ocurrio un error: {}", ex);
						CodeError = "9999";
						DescError = "RECHAZO BANCARIO, POR FAVOR VERIFICA TUS DATOS";
						ex.printStackTrace();
					}
				}
				/*modificacion nueva arquitectura mybatis JCDP revisar LOG */
				compraResponce = responseCompraIAVE_2(bitacora, response, compraiave,
						Double.parseDouble(producto.getPro_monto()), Double.parseDouble(Comision), usuario, CodeError,
						DescError, Resbanco);
			} catch (ParserConfigurationException ex) {
				LOGGER.error("Ocurrio un error: {}", ex);
				ex.printStackTrace();
			} catch (IOException ex) {
				LOGGER.error("Ocurrio un error: {}", ex);
				ex.printStackTrace();
			} catch (SAXException ex) {
				LOGGER.error("Ocurrio un error: {}", ex);
				ex.printStackTrace();
			} catch (Exception e) {
				LOGGER.error("Ocurrio un error: {}", e);
				e.printStackTrace();
			}
			} // fin de if principal
		
			/*****************************************/
			else {
				LOGGER.info("/*************************** USUARIO INEXISTENTE !!!!!! ********************************/");				
			}
	    }
		respuesta = gson.toJson(compraResponce);
	        LOGGER.info("Respuesta Recarga IAVE: " + respuesta );
		json = Crypto.aesEncrypt(key, respuesta);
            return res;
        }
    
        public boolean esTuTag(String idProducto) {
		String productosTuTag = null;
		String[] productosTuTagArr = null;
		boolean resultado = false;
		try {
			productosTuTag = consultaParametros("@PRODUCTOS_TUTAG");
			productosTuTagArr = productosTuTag.split("\\|");
			for (String s : productosTuTagArr) {
				if (s.equalsIgnoreCase(idProducto)) {
					resultado = true;
					break;
				}
			}
		} catch (Exception e) {
			LOGGER.error("Ocurrio un error: {}", e);
		}
		return resultado;
	}
        
        public CompraResponce responseCompraIAVE_2(BitacoraVO bitacora, String response, CompraIAVE compra, double Cargo,
			double Comision, UsuarioVO usuario, String CodeError, String DescError, ProsaResponse Resbanco)
					throws ParserConfigurationException, IOException, SAXException,
					ClassNotFoundException {
		CompraResponce compraResponce = new CompraResponce();
                //LOGGER.debug("response : " + response);
		try {
			compraResponce.setReferencia(bitacora.getId_bitacora());
			if (response.equals("ERROR")) {
				if (CodeError == null) {
					CodeError = "9999";
					DescError = "RECHAZO PROSA";
				}
				bitacora.setBit_codigo_error(Integer.parseInt(CodeError));
				bitacora.setBit_ticket("Excepcion: " + DescError);
				bitacora.setBit_status(0);
			        actualizaBitacora(bitacora);
				compraResponce.setResultado(0);
                                LOGGER.debug("bitacora.getId_bitacora() : " + bitacora.getId_bitacora());
				compraResponce.setFolio("" + bitacora.getBit_codigo_error());
				compraResponce.setMensaje(getErrorByClave(compraResponce.getFolio()));
				if ((compraResponce.getMensaje() == null) || (compraResponce.getMensaje().length() == 0)) {
					compraResponce.setMensaje("Excepcion: " + DescError);
				}
				return compraResponce;
			}
			if (response.equals("ERRORIAVE")) {
				//bitacora.setCodigo_error(Integer.parseInt("881"));
                                LOGGER.debug("bitacora.getId_bitacora() : " + bitacora.getId_bitacora());
				bitacora.setBit_codigo_error(Integer.parseInt("881"));
				bitacora.setBit_ticket("Excepcion: servicio no disponible de I+D al consultar saldo");
				bitacora.setBit_status(0);
				actualizaBitacora(bitacora);
				compraResponce.setResultado(0);
				compraResponce.setFolio("881");
				compraResponce.setMensaje(getErrorByClave(compraResponce.getFolio()));
				//JCDP
				if ((compraResponce.getMensaje() == null) || (compraResponce.getMensaje().length() == 0)) {
					compraResponce.setMensaje("el servicio de recarga I+D NO esta disponible.");
				}
				return compraResponce;
			}
			/*******************/
			if(response.equals("ERRORREVERSE"))
			{
                                //LOGGER.debug("bitacora.getId_bitacora() : " + bitacora.getId_bitacora());
				bitacora.setBit_codigo_error(Integer.parseInt(CodeError));
				bitacora.setBit_ticket("Excepcion: " + DescError);
				bitacora.setBit_status(0);
				actualizaBitacora(bitacora);
				compraResponce.setResultado(0);
				compraResponce.setFolio(CodeError);
				compraResponce.setMensaje(getErrorByClave(compraResponce.getFolio()));
				//JCDP
				if ((compraResponce.getMensaje() == null) || (compraResponce.getMensaje().length() == 0)) {
					compraResponce.setMensaje(DescError);
				}
				return compraResponce;
			}
			/*******************/
			RespuestaIAVE Res = ParceXMLIAVE(response);
			try {
				bitacora.setBit_no_autorizacion(Res.getAutono());
			} catch (Exception e) {
				bitacora.setBit_no_autorizacion("0");
			}
			String ticket = "COMPRA TAG I+D AUTORIZACION: " + Res.getAutono() + " TAG: " + compra.getTarejeta()
					+ " RECARGA DE: " + Cargo + " COMISION: " + Comision;
			bitacora.setBit_ticket(ticket);
			bitacora.setBit_no_autorizacion(bitacora.getBit_no_autorizacion());
			bitacora.setBit_status(1); 
			actualizaBitacora(bitacora);
			BitacoraiaveVO  bitacoraiave = new BitacoraiaveVO();
			bitacoraiave.setId_bitacora(bitacora.getId_bitacora());
			bitacoraiave.setId_grp(Res.getId_grp());
			bitacoraiave.setCard_number(Res.getCard_number());
			bitacoraiave.setCheck_digit(Res.getCheck_digit());
			bitacoraiave.setLocal_date(Res.getLocal_date());
			bitacoraiave.setAmount(Res.getAmount());
			bitacoraiave.setAutono(Res.getAutono());
			bitacoraiave.setResponsecode(Res.getResponsecode());
			bitacoraiave.setDescriptioncode(Res.getDescriptioncode());
			bitacoraiave.setTrx_no(Res.getTrx_no());
			actualizaBitacoraIave(bitacoraiave);
			if (Res.getResponsecode().equals("00")) {
				compraResponce.setResultado(1);
				compraResponce.setMensaje("Recarga TAG I+D exitosa. ");
				compraResponce.setFolio("" + Res.getAutono());
				compraResponce.setNumAutorizacion(Resbanco.getAutorizacion());
				compraResponce.setCargo(Cargo);
				compraResponce.setComision(Comision);
				new ParametroDAO().EnviaMailIAVE(usuario.getUsr_nombre(), usuario.geteMail(), consultaParametros("@MENSAJE_COMPRAIAVE"),
						consultaParametros("@ASUNTO_COMPRAIAVE"), "", "", "" + Cargo, Resbanco.getAutorizacion(), "" + Comision, "",
						Res.getAutono(), compra.getTarejeta(), String.valueOf(bitacora.getId_bitacora()));
			} else {
				compraResponce.setResultado(Integer.parseInt(Res.getResponsecode()));
				compraResponce.setMensaje(Res.getDescriptioncode());
				compraResponce.setFolio("0");
				bitacora.setBit_codigo_error(Integer.parseInt(Res.getResponsecode()));
				bitacora.setBit_ticket(Res.getDescriptioncode());
				//new BitacoraDAO().updateIAVE(bitacora, false, Res.getDescriptioncode(), 1);
				actualizaBitacora(bitacora);
			}
			//JCDP
			/*se realiza el insert con my batis*/
			BitacoraprosaVO bitprosa = new BitacoraprosaVO();
			bitprosa.setId_bitacora(Long.parseLong(Integer.toString(bitacora.getId_bitacora())));
			bitprosa.setId_usuario(bitacora.getId_usuario());
			bitprosa.setTarjeta("");
			bitprosa.setTransaccion("");
			bitprosa.setAutorizacion(Resbanco.getAutorizacion());
			bitprosa.setConcepto(bitacora.getBit_concepto());
			bitprosa.setTarjeta(Res.getTrx_no() + "-" + Res.getAutono());
		    bitprosa.setCargo(Long.parseLong(Double.toString(Cargo)) + Long.parseLong(Double.toString(Comision)));
		    bitprosa.setComision(Long.parseLong(Double.toString(Comision)));
		    bitprosa.setCx(compra.getCx());
		    bitprosa.setCy(compra.getCy());
		    insertaBitacoraProsa(bitprosa);
		} catch (Exception e) {
			LOGGER.debug("ERROR [responseCompraIAVE]: " + e.toString());
			bitacora.setBit_codigo_error(99999999);
			bitacora.setBit_ticket(bitacora.getBit_ticket());
			bitacora.setBit_no_autorizacion(bitacora.getBit_no_autorizacion());
			bitacora.setBit_status(0);
			actualizaBitacora(bitacora);
			compraResponce.setResultado(0);
			compraResponce.setFolio("" + bitacora.getBit_codigo_error());
			//JCDP
			//compraResponce = new MensajeDAO().getErrorByClave(compraResponce);
			compraResponce.setMensaje(getErrorByClave(compraResponce.getFolio()));
			//JCDP
			if ((compraResponce.getMensaje() == null) || (compraResponce.getMensaje().length() == 0)) {
				compraResponce.setMensaje("error en compra");
			}
		}
		return compraResponce;
	}
        
        public CompraResponce validaReglasNegocio(long idUsuario, String tarjeta, int idProducto) {
		CompraResponce compraResponce = null;
		int numUsuario = 0;
		int montoUsuario = 0;
		int numTarjeta = 0;
		int montoTarjeta = 0;

		Map<String, Object> valoresU = null;
		Map<String, Object> valoresT = null;
		try {
			numUsuario = Integer.parseInt(consultaParametros("@IMASD_NUM_TRAN_USUARIO"));
			montoUsuario = Integer.parseInt(consultaParametros("@IMASD_MONTO_USUARIO"));
			numTarjeta = Integer.parseInt(consultaParametros("@IMASD_NUM_TRAN_TARJETA"));
			montoTarjeta = Integer.parseInt(consultaParametros("@IMASD_MONTO_TARJETA"));
			valoresU = seleccionarRegla(idUsuario, null, "5,8,17", "@IMASD_DIAS_TRAN_USUARIO");
			valoresT = seleccionarRegla(0L, tarjeta, "5,8,17", "@IMASD_DIAS_TRAN_TARJETA");
			if (((Integer) valoresU.get("numero")).intValue() >= numUsuario) {
				compraResponce = new CompraResponce(0, "920",
						"En el periodo " + valoresU.get("fechaInicio") + " a " + valoresU.get("fechaFin")
								+ " a superado el nÃºmero maximo de transacciones permitidas: " + numUsuario);
			} else if (((Integer) valoresU.get("monto")).intValue() >= montoUsuario) {
				compraResponce = new CompraResponce(0, "921",
						"En el periodo " + valoresU.get("fechaInicio") + " a " + valoresU.get("fechaFin")
								+ " a superado el Monto maximo permitido: $ " + U.formatoImporte(montoUsuario));
			} else if (((Integer) valoresT.get("numero")).intValue() >= numTarjeta) {
				compraResponce = new CompraResponce(0, "922",
						"En el periodo " + valoresT.get("fechaInicio") + " a " + valoresT.get("fechaFin")
								+ " a superado el nÃºmero maximo de transacciones permitidas para una Tarjeta: "
								+ numTarjeta);
			} else if (((Integer) valoresT.get("monto")).intValue() >= montoTarjeta) {
				compraResponce = new CompraResponce(0, "923",
						"En el periodo " + valoresT.get("fechaInicio") + " a " + valoresT.get("fechaFin")
								+ " a superado el Monto maximo permitido para una Tarjeta: $ "
								+ U.formatoImporte(montoTarjeta));
			}
		} catch (Exception e) {
			LOGGER.error("Ocurrio un error al validar Reglas de Negocio.", e);

			compraResponce = new CompraResponce(0, "920", "Ocurrio un error al validar Reglas de Negocio.");
		}
		return compraResponce;
	}
        
        public RespuestaIAVE ParceXMLIAVE(String cadena) {
		String sXML = cadena;
		RespuestaIAVE oR = new RespuestaIAVE();
		try {
			//File file = new File("response.xml");
			//BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			sXML = sXML.replace("<![CDATA[", "");
			sXML = sXML.replace("]]>", "");
			sXML = sXML.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
			sXML = sXML.replace("á", "a");
			sXML = sXML.replace("é", "e");
			sXML = sXML.replace("í", "i");
			sXML = sXML.replace("ó", "o");
			sXML = sXML.replace("ú", "u");

			sXML = sXML.replace("�?", "A");
			sXML = sXML.replace("É", "E");
			sXML = sXML.replace("í", "I");
			sXML = sXML.replace("Ó", "O");
			sXML = sXML.replace("Ú", "U");

			//bw.write(sXML);
			//bw.close();
			InputStream is = new ByteArrayInputStream(sXML.getBytes());
					
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			Document doc = db.parse(is); //file
			doc.getDocumentElement().normalize();

			NodeList Res = doc.getElementsByTagName("ReloadResponse");
			Node firstPersonNode1 = Res.item(0);
			Node firstPersonNode2 = Res.item(0);
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("ID_GRP");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setId_grp(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode2.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode2;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("CARD_NUMBER");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setCard_number(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("CHECK_DIGIT");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setCheck_digit(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("LOCAL_DATE");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setLocal_date(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("AMOUNT");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setAmount(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("TRX_NO");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setTrx_no(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("AUTONO");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setAutono(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("RESPONSECODE");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setResponsecode(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("DESCRIPTIONCODE");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setDescriptioncode(textFNList.item(0).getNodeValue().trim());
				}
			}
			
		} catch (Exception ex) {
			LOGGER.debug("ERROR [PARCEXML IAVE: ]: " + ex.toString());
			oR.setResponsecode("9999");
			oR.setDescriptioncode("ERROR- Banco (1)");
			
		} finally {
			return oR;
		}
	}
        
        private String Ceros(String cad, int NumCeros) {
		String tmp = "00000000000000000000000000000";
		return (tmp + cad).substring((tmp + cad).length() - NumCeros, (tmp + cad).length());
	}
        
        private String executePurchaseIAVERecarga(String folio, String tag, String monto, String DV, String idcom,
			String idgrp, String ActivaTrans) {
		String Res = null;
		try {
			//LOGGER.info("Dentro de executePurchaseIAVERecarga Entro 1");
			Calendar mC = Calendar.getInstance();

			String wfecha = Ceros(new StringBuilder().append("").append(mC.get(5)).toString(), 2) + "/"
					+ Ceros(new StringBuilder().append("").append(mC.get(2) + 1).toString(), 2) + "/" + mC.get(1) + " "
					+ Ceros(new StringBuilder().append("").append(mC.get(11)).toString(), 2) + ":"
					+ Ceros(new StringBuilder().append("").append(mC.get(12)).toString(), 2) + ":"
					+ Ceros(new StringBuilder().append("").append(mC.get(13)).toString(), 2);

			String prefijo = "";
			String puntos = "";
			if (tag.length() != 11) {
				while (tag.length() < 8) {
					tag = "0" + tag;
				}
				if ((tag.length() == 8) && (Long.parseLong(tag) >= 20000000L)) {
					prefijo = "IMDM";
				}
				if ((tag.length() == 8) && (Long.parseLong(tag) < 20000000L)) {
					prefijo = "CPFI";
				}
				if (tag.length() == 8) {
					puntos = "..";
				}
			}
			//LOGGER.info("Dentro de executePurchaseIAVERecarga Entro 2");
			StringBuffer pXML = new StringBuffer().append("<ReloadRequest>").append("  <ID_COM>").append(idcom)
					.append("</ID_COM>").append("  <ID_GRP>").append(idgrp).append("</ID_GRP>")
					.append("  <CARD_NUMBER>").append(prefijo).append(tag).append(puntos).append("</CARD_NUMBER>")
					.append(" <CHECK_DIGIT>").append(DV).append("</CHECK_DIGIT>").append("  <LOCAL_DATE>")
					.append(wfecha).append("</LOCAL_DATE>").append("  <AMOUNT>").append(monto).append("</AMOUNT>")
					.append(" <TRX_NO>").append(folio).append("</TRX_NO>").append("</ReloadRequest>");

			LOGGER.debug("MPX COMPRA IAVE : " + pXML);
			//LOGGER.info("Dentro de executePurchaseIAVERecarga Entro 3");
			if (ActivaTrans.equals("1")) {
				//LOGGER.info("Dentro de executePurchaseIAVERecarga Entro 4");
				WsPrepagoRecargasSoapProxy proxy = new WsPrepagoRecargasSoapProxy();
				Res = proxy.reload(pXML.toString());
			} else {
				LOGGER.debug("Ambiente QA, se genera una respuesta");
				Res =  new U().XMLTestIAVE();
			}
			
			return Res;
		} catch (Exception ex) {
			LOGGER.error("MPX COMPRA IAVE ERROR: " + ex.toString());
		}
		return "";
	}
        
        /****************************************************************************************************************************/
	public String consultaServicios(String json) {
		List<ServiciosAntadResponse> serviciosAntadList = null;
		ServiciosAntadRequest request = null;
		try {
			json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_SERVICIOS + json);
			request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			serviciosAntadList = mapper.consultaServiciosAntad(request);
			json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_SERVICIOS+"["+e.getCause()+"]");
			json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_SERVICIOS+json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return json;
	}

	/*****************************************/
	/*nuevo metodo de consulta de parametros */
	/*****************************************/
	public String consultaParametros(String json) {
		String  parametro = "";
		ServiciosAntadRequest request = null;
		try {
			//json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_PARAMETROS + json);
			//request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			parametro = mapper.findByParametro(json);
			//json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_PARAMETROS+"["+e.getCause()+"]");
			json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_PARAMETROS+parametro);
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return parametro;
	}
	
	/*****************************************/
	/*nuevo metodo que ontiene un error por id_error*/
	/*****************************************/
	public String getErrorByClave(String id_error) {
		String  parametro = "";
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_DESC_ERROR + id_error);
			parametro = mapper.getErrorByClave(id_error);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_DESC_ERROR+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_DESC_ERROR+id_error);
		}
		return parametro;
	}
        
     
        
	
	
	/*****************************************/
	/*nuevo metodo de consulta que valida usuario por usuario y password */
	/*****************************************/
	public UsuarioVO validaUsuario(String user, String password) {
		UsuarioVO usuario = new UsuarioVO();
		//ServiciosAntadRequest request = null;
		try {
			LOGGER.info("Entro validaUsuario 1");
			//json = AddcelCrypto.decryptSensitive(json);
			//LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO + json);
			//request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			usuario = mapper.validaUsuario(user, password);
			LOGGER.info("Entro validaUsuario 2");
		    LOGGER.info(LOG_SERVICE+usuario);
			//json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.info("Entro validaUsuario 3");
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO+"["+e.getCause()+"]");
			//json = JSON_ERROR_INESPERADO;
		} finally{
			//LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO+json);
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		LOGGER.info("Entro validaUsuario 4");
		return usuario;
	}
	
	/*****************************************/
	/*nuevo metodo de consulta que consulta producto por clave proporcionada */
	/*****************************************/
	public ProductoVO consultaProducto(String producto) {
		ProductoVO prod = new ProductoVO();
		String  parametro = "";
		//ServiciosAntadRequest request = null;
		try {
			//json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO + producto);
			//request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			prod = mapper.getProductoByClaveWS(producto);
		    LOGGER.info(LOG_SERVICE+prod);
		    parametro = prod.getNombre();
		    LOGGER.info(LOG_SERVICE+parametro);
			//json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO+"["+e.getCause()+"]");
			producto = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO+producto);
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return prod;
	}
	
	/*****************************************/
	/*nuevo metodo de consulta que consulta producto por clave proporcionada */
	/*****************************************/
	public ProveedorVO consultaProveedor(String idproveedor) {
		ProveedorVO proveedor = new ProveedorVO();
		String  parametro = "";
		//ServiciosAntadRequest request = null;
		try {
			//json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_PROVEEDOR + idproveedor);
			//request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			proveedor = mapper.getProveedor(idproveedor);
		    LOGGER.info(LOG_SERVICE+proveedor);
		    parametro = proveedor.getPrv_nombre_comercio();
		    LOGGER.info(LOG_SERVICE+parametro);
			//json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_PROVEEDOR+"["+e.getCause()+"]");
			idproveedor = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_PROVEEDOR+idproveedor);
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return proveedor;
	}
	
	/*****************************************/
	/*nuevo metodo de consulta que consulta la comision por clave de proveedor */
	/*****************************************/
	public double consultaComision(String idproveedor) {
		double parametro = 0;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_COMISION + idproveedor);
			parametro = mapper.getComision(Integer.parseInt(idproveedor));
		    LOGGER.info(LOG_SERVICE+parametro);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_COMISION+"["+e.getCause()+"]");
			idproveedor = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_COMISION+idproveedor);
		}
		return parametro;
	}
	
	/*****************************************/
	/*nuevo metodo que actualiza la bitacora */
	/*****************************************/
	public void actualizaBitacora(BitacoraVO bitacora) {
		//BitacoraVO bitacora = new BitacoraVO();
		//String  parametro = "Bitacora actualizada";
		//bitacora.setId_bitacora(362);
		//bitacora.setBit_concepto("Compra TA Telcel 5534918527 20.00 modificado JCDP");
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_ACTUALIZA_BITACORA + bitacora.getId_bitacora());
			mapper.actualizaBitacora(bitacora);
		    //LOGGER.info(LOG_SERVICE+bitacora);
		    //LOGGER.info(LOG_SERVICE+parametro);
			//json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_ACTUALIZA_BITACORA+"["+e.getCause()+"]");
			//json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_ACTUALIZA_BITACORA);
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		//return parametro;
	}
	
	/*****************************************/
	/*nuevo metodo validaT */
	/*****************************************/
	public boolean validaT(String T) {
		boolean Res=false;
        int resultado = 0;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_VALIDA_T + T);
			resultado = mapper.validaT(T);
			LOGGER.info("resultado :" + resultado);
            if(resultado == 1){
                Res=true;
            } 
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_VALIDA_T+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_VALIDA_T+Res);
		}
		return Res;
	}
	
	/*****************************************/
	/*nuevo metodo que actualiza la bitacoraiave */
	/*****************************************/
	public void actualizaBitacoraIave(BitacoraiaveVO bitacoraiave) {
		/*BitacoraiaveVO bitacoraiave = new BitacoraiaveVO();
		String  parametro = "Bitacoraiave actualizada";
		bitacoraiave.setId_bitacora(10763);
		bitacoraiave.setPosname("mofificacion de prueba by JCDP");*/
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_ACTUALIZA_BITACORAIAVE);
			mapper.actualizaBitacoraIave(bitacoraiave);
		    LOGGER.info(LOG_SERVICE+bitacoraiave);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_ACTUALIZA_BITACORAIAVE+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_ACTUALIZACION_BITACORAIAVE);
		}
	}
	
	/*****************************************/
	/*nuevo metodo que inserta la bitacoraiave */
	/*****************************************/
	public void insertaBitacoraIave(BitacoraiaveVO bitvo) {
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_INSERTA_BITACORAIAVE);
			mapper.insertaBitacoraIave(bitvo);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_INSERTA_BITACORAIAVE+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_BITACORAIAVE);
		}
	}
	
	/*****************************************/
	/*nuevo metodo que inserta la bitacora prosa */
	/*****************************************/
	public void insertaBitacoraProsa(BitacoraprosaVO bitprosa) {
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_INSERTA_BITACORA_PROSA);
			mapper.insertaBitacoraProsa(bitprosa);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_INSERTA_BITACORA_PROSA+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_INSERTA_BITACORA_PROSA);
		}
	}
	

	/*****************************************/
	/*nuevo metodo consulta folio iave */
	/*****************************************/
	public String getFolioIAVE() {
		String res = ""; 
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTAFOLIO_IAVE);
			res = mapper.getFolioIAVE();
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTAFOLIO_IAVE+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_FOLIO_IAVE+res);
		}
		return res;
	}
	
	/*****************************************/
	/* nuevo metodo consulta folio banco iave */
	/*****************************************/
	public String getFolioBancoIAVE() {
		String res = ""; 
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTAFOLIO_BANCO_IAVE);
			res = mapper.getFolioBancoIAVE();
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTAFOLIO_BANCO_IAVE+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_FOLIO_BANCO_IAVE+res);
		}
		return res;
	}

    /*****************************************/
	/* nuevo metodo consulta estados */
	/*****************************************/
	public List<EstadosVO> getEstados() {
                List<EstadosVO> res = null;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTAESTADOS);
			res = mapper.getEstados();
                        LOGGER.info("numero de estados encontrados : " + res.size());
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTAESTADOS+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_ESTADOS+res);
		}
		return res;
	}
	
	/*****************************************/
	/* nuevo metodo consulta tipos de tarjeta */
	/*****************************************/
	public List<TTipoTarjetaVO> getTiposTarjeta() {
                List<TTipoTarjetaVO> res = null;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTATIPOSTARJETAS);
			res = mapper.getTiposTar();
                        LOGGER.info("numero de tarjetas encontradas : " + res.size());
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTATIPOSTARJETAS+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_TIPOSTARJETAS+res);
		}
		return res;
	}
	
	/*****************************************/
	/* nuevo metodo consulta bancos */
	/*****************************************/
	public List<TBancoVO> getBancos() {
                List<TBancoVO> res = null;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_BANCOS);
			res = mapper.getBancos();
                        LOGGER.info("numero de bancos encontrados : " + res.size());
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_BANCOS+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_BANCOS+res);
		}
		return res;
	}
	
	/*****************************************/
	/* nuevo metodo consulta bancos */
	/*****************************************/
	public List<Proveedor> getProveedores() {
                List<Proveedor> res = null;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_CARRIES);
			res = mapper.getProveedoresCarries();
            LOGGER.info("numero de proveedores : " + res.size());
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_CARRIES+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_CARRIES+res);
		}
		return res;
	}
	/*metodo para poder consultar los proveedores */
	 public List getProveedores(String IdCategorias, long idUsuario, long plataforma) {
	        List proveedores = new ArrayList();
	        UsuarioVO usu = new UsuarioVO();
	         int tipotarjeta = 0;
	        try {
	            LOGGER.debug("MPX IDCATEGORIA: " + IdCategorias);
	            tipotarjeta = mapper.getTipoTarByUsuario(idUsuario);
	        } catch (Exception ex) {
	            LOGGER.error("Ocurrio un error getProveedores(String, long, long): {}",ex);
	        } finally { 
	        	LOGGER.info("getProveedores() : OK 1");
	        }
	        try{
	            String sWhere = "";
	            if (plataforma == 1) { //IOS
	                sWhere = "  and ActivoIOS = 1 ";
	            }
	            if (plataforma == 2) { //Android
	                sWhere = "  and ActivoAndroid = 1 ";
	            }
	            if (plataforma == 3) { //BB
	                sWhere = "  and ActivoBB = 1 ";
	            }
	            if (plataforma == 4) { //WPhone
	                sWhere = "  and ActivoWPhone = 1 ";
	            }
	            if (plataforma == 5) { //Java
	                sWhere = "  and ActivoJava = 1 ";
	            }
	            
	            /*statement = connect().prepareStatement( SELECT_TPROVEEDOR_IDC + sWhere);
	            statement.setString(1, IdCategorias);
	            resultSet = statement.executeQuery();
	            if (resultSet.next()) {
	                do {
	                    proveedores.add(getProveedorRS_amex(resultSet, tipotarjeta));
	                } while (resultSet.next());
	            }*/
	        } catch (Exception ex) {
	            LOGGER.error("Ocurrio un error: {}",ex);
	        } finally {
	            LOGGER.info("getProveedores() : OK 2");
	        }
	        return proveedores;
	    }
	
	// INI pDiaz
	public String consultaEndPointsService(String json) {
				
		List<EndPointsResponse> serviciosEndPointsList = null;
		EndPointsRequest request = null;
		
		try {
			//json = AddcelCrypto.decryptSensitive(json);
			//LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_SERVICIOS + json);
			request = (EndPointsRequest) jsonUtils.jsonToObject(json, EndPointsRequest.class);
			serviciosEndPointsList = mapper.consultaEndPoints(request);
			json = (String) jsonUtils.objectToJson(serviciosEndPointsList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_SERVICIOS+"["+e.getCause()+"]");
			json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_SERVICIOS+json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return json;
	}
	// FIN pDiaz

	public Map<String, Object> seleccionarRegla(long idUsuario, String tarjeta,
			String proveedor, String clave) {
		// TODO Auto-generated method stub
		String  parametro = "";
		//ServiciosAntadRequest request = null;
		Map<String, Object> valoresU = null;
		try {
			//json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_SELECCIONAR_REGLAS + idUsuario);
			//request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			valoresU = mapper.seleccionarRegla(idUsuario, tarjeta, proveedor, clave);
			//json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_SELECCIONAR_REGLAS+"["+e.getCause()+"]");
			//"idUsuario = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_REGLAS+idUsuario);
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		LOGGER.info("EL MAP contiene : " + valoresU.size());
		return valoresU;
	}
	
	/*nuevo metodo para actualizar usuarios*/
	public void actualizaUsuario(UsuarioVO usuario){
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_ACTUALIZA_USUARIO + usuario.getUsr_login());
			mapper.updateUsuarios(usuario);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_ACTUALIZAR_USUARIO+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_ACTUALIZA_USUARIO+usuario.getUsr_login());
		}
	}
        
	/*
	 * nuevo metodo en sustitucion del anterior metodo con el mismo nombre
	 * by JCDP 15072016
	 * 
	 * */
	public void  BuscaIm (String T) {
		BloqImeiVO bi = new BloqImeiVO(); 
		bi = mapper.selBloqImei(T);
	        try{
	        	if(T.trim().equals("")) return;
	            if ( bi != null) {
	            	LOGGER.debug("BUSCA IMEI ACTUALIZA: " + T );
	                bi.setActivo(1);
	                mapper.updateBloqImei(bi);
	            }else{
	               bi.setActivo(1);
	               mapper.insertBloqImei(bi);
	               LOGGER.debug("BUSCA IMEI ALTA: " + T );
	            }
	        }catch(Exception e){
	        	LOGGER.error("Ocurrio un error en nuevo metodo BuscaIm: {}", e);
	        }
    }
	
	/*
	 * metodo ValidaIM con la nueva arquitectura de MyBatis 
	 * By JCDP 
	 * 15072016 
	 * */
	public boolean  ValidaIM (String T) {
        boolean res=false;
        String act = mapper.bImei(T);
        LOGGER.info("act : -> " + act);
        try {
            if(act != null){
                res=true;
            }
        } catch (Exception ex) {
        	LOGGER.error("Ocurrio un error en metodo ValidaIM: {}",ex.getMessage());
        }
         return res;
    }
	/*
	 * metodo BTar con la nueva arquitectura de MyBatis 
	 * By JCDP 
	 * 15072016 
	 * */
	public void BTar(String tarjeta){
		TBloVO  tb = new TBloVO(); 
        try{
            if(tarjeta.trim().equals("")) return;
            tb = mapper.getTBloByTarjeta(ParametroDAO.setSMS( tarjeta));
            if ( tb != null ){
            	LOGGER.debug("BUSCA TARJETA ACTUALIZA: "  );
                tb.setActivo(1);
                tb.setTarjeta(U.setSMS(tarjeta));
                mapper.updateTBlo(tb);
            }else{
            	TBloVO  tb1 = new TBloVO();
            	LOGGER.debug("BUSCA TARJETA ALTA: "  );
            	tb1.setActivo(1);
            	tb1.setTarjeta(U.setSMS( tarjeta));
                mapper.updateTBlo(tb1);
            }
        }catch(Exception e){
        	LOGGER.error("Ocurrio un error en metodo BTar: {}", e.getMessage());
        }
    }
}
