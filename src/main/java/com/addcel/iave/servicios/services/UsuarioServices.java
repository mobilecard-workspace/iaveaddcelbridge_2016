/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.iave.servicios.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.iave.integration.ParametroDAO;
import com.addcel.iave.model.mapper.IaveServiciosMapper;
import com.addcel.iave.model.vo.Contenedor;
import com.addcel.iave.model.vo.Producto;
import com.addcel.iave.model.vo.ProveedorVO;
import com.addcel.iave.model.vo.Respuesta;
import com.addcel.iave.model.vo.UserPasswordUpdate;
import com.addcel.iave.model.vo.Usuario;
import com.addcel.iave.model.vo.UsuarioTagsVO;
import com.addcel.iave.model.vo.UsuarioVO;
import static com.addcel.iave.utils.Constantes.JSON_ERROR_INESPERADO;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_AGREGAR_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTA_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTA_USUARIO_EMAIL;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTA_USUARIO_IMEI;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTA_USUARIO_TDC;
import static com.addcel.iave.utils.Constantes.LOG_ERROR_CONSULTA_USUARIO_TELEFONO;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_ADD_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_USUARIO_EMAIL;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_USUARIO_IMEI;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_USUARIO_TDC;
import static com.addcel.iave.utils.Constantes.LOG_PROCESO_CONSULTA_USUARIO_TELEFONO;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_USUARIO_EMAIL;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_USUARIO_IMEI;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_USUARIO_TDC;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_CONSULTA_USUARIO_TELEFONO;
import static com.addcel.iave.utils.Constantes.LOG_RESPUESTA_OBTENER_USUARIO;
import static com.addcel.iave.utils.Constantes.LOG_SERVICE;
import com.addcel.iave.utils.U;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import crypto.Crypto;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;

/**
 *
 * @author JCDP
 */
@Service
public class UsuarioServices {
        
	    private Gson gson = new Gson();
        private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioServices.class);
        
         @Autowired
	public UtilsServiciosService utilsService;
        
        @Autowired
	private IaveServiciosMapper mapper;
           /**********************************Servicio Insertar Usuario***********************************************/
         public String insertaUsuarioService(String jsonEnc){
            LOGGER.info("Inicio INSERT USUARIO... " );   
         String key = "1234567890ABCDEF0123456789ABCDEF";
            Respuesta respuesta = new Respuesta();
            String resp = null;
            Usuario usuario  = new Usuario();

            String json;
            
            if(jsonEnc == null ) {
			LOGGER.info("parameter json null " );
		}
		if(jsonEnc.equals("")){
		    json = (String) jsonEnc.toString();
		    LOGGER.info("Dentro de If en insertaUsuario " );
		}else{
		    json = (String) jsonEnc.toString();
		    LOGGER.info("Dentro de else en insertaUsuario " );
		}

		    LOGGER.info("Inicio INSERT USUARIO... " );
            LOGGER.info("CADENA userInsert " + json);
            String p=usuario.getPassword();
            //String p=usuario.getUsr_pwd();

            String [] Cad = new com.addcel.iave.integration.ParametroDAO().Asf(json);
            key = Cad[0];
            json = Cad[1];
            p = Cad[2];

            Type collectionType = new TypeToken<Usuario>(){}.getType();
            usuario = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);
            usuario.setPassword(p);

            LOGGER.info("REGISTRO USUARIO" );
            LOGGER.info("Usuario: " + usuario.getLogin() );
            LOGGER.info("Tipo: " + usuario.getTipo() );
            LOGGER.info("Software: " + usuario.getSoftware());
            LOGGER.info("Modelo: " + usuario.getModelo());
            LOGGER.info("Imei: " + usuario.getImei());
            LOGGER.info("NOMBRE: " + usuario.getNombre() + " " + usuario.getApellido()+ " " + usuario.getMaterno());
            LOGGER.info("fecha nacimiento: " + usuario.getNacimiento());

            if(!existeUsuario(usuario)){
            	LOGGER.info("Usuario no existe : " + usuario.getLogin() );    
                if(utilsService.ExisteMail(usuario)){
                   respuesta.setResultado(3);
                   respuesta.setMensaje("El Correo Electronico ya fue asignado a otro usuario. Intente de nuevo.");

                }else if(utilsService.ExisteT(usuario)){
                    respuesta.setResultado(4);
                    respuesta.setMensaje("La tarjeta proporcionada ya esta asignada a otro usuario. Intente de nuevo.");

                }else if(utilsService.ExisteIMEI(usuario)){
                    respuesta.setResultado(5);
                    respuesta.setMensaje("No es posible registrar el usuario.");
                    LOGGER.info("ERR-00 -> El IMEI ya se encuentra registrado en la base de datos. IMEI= " + usuario.getImei() + "   Usuario = " + usuario.getLogin());

                }else if(utilsService.ExisteTelefono(usuario)){
                    respuesta.setResultado(5);
                    respuesta.setMensaje("El telefono ya esta asignado a otro usuario. Intente de nuevo.");

                }else if(addUsuario(usuario)){
                    respuesta.setResultado(1);
                    if(usuario.getTipo().toUpperCase().equals("IPAD")  || usuario.getTipo().toUpperCase().equals("IPOD") ){
                          respuesta.setMensaje("El sistema te envio un E-MAIL con la clave de acceso para completar tu registro.");
                    }else{
                          respuesta.setMensaje("El sistema te envio un SMS o un E-MAIL con la clave de acceso para completar tu registro.");
                    }              
                }else{
                   respuesta.setResultado(2);
                   respuesta.setMensaje("Ha ocurrido un error, intentelo nuevamente");
                }

            }else{
                respuesta.setResultado(2);
                respuesta.setMensaje("El usuario ya existe");
            }
              resp = gson.toJson(respuesta);
              LOGGER.info("Respuesta UserInsert: " + resp );
              resp = Crypto.aesEncrypt(key, resp);
            return resp;
         }
        
         /**********************************Servicio Usuario Login***********************************************/
        public String usuarioLoginService(String jsonEnc){
            LOGGER.info("**********************************   LOGIN");
            String key = "1234567890ABCDEF0123456789ABCDEF";
            String resp = null;
            Respuesta respuesta = new Respuesta();
            Usuario usuario = null;
            String json;
            if (jsonEnc == null) {
                LOGGER.info("parameter json null ");
            }
            if (jsonEnc.equals("")) {
                json = (String) jsonEnc.toString();
                LOGGER.info("Dentro de If en usuarioLogin ");
            } else {
                json = (String) jsonEnc.toString();
                LOGGER.info("Dentro de else en usuarioLogin ");
            }
            LOGGER.info("CADENA userLogin: " + json);

            
            try {
                String[] Cad = new com.addcel.iave.integration.ParametroDAO().Asf(json);
                key = Cad[0];
                json = Cad[1];

                Type collectionType = new TypeToken<Usuario>() {
                }.getType();
                usuario = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);
                LOGGER.info("usuario" + usuario.getUsuario());
                LOGGER.info("usuario password" + usuario.getPassword());
                LOGGER.info("usuario passwordS" + usuario.getPasswordS());
            } catch (Exception e) {
                LOGGER.error("ERROR al desifrar JSON: " + e.getMessage());
                respuesta.setResultado(2);
                respuesta.setMensaje("Usuario Invalido. Verifique el usuario y/o password");
            }
            if (usuario != null) {
                LOGGER.info("CADENA USUARIO 1: " + usuario.getLogin());

                usuario.setPassword(Crypto.aesEncrypt(key, usuario.getPassword()));

                LOGGER.info("Login: " + usuario.getUsuario());
                //LOGGER.info("CADENA USUARIO 2, Usuario:" + usuario.getNombre() + " *** Password: " + usuario.getPassword());
                LOGGER.info("CADENA USUARIO 2, Usuario:" + usuario.getNombre() + " *** Password: " + usuario.getPassword());

                UsuarioVO RU = consultaUsuarioById(usuario.getLogin());
                if (RU != null && RU.getUsr_nombre() != null) {
                    //LOGGER.info("CADENA USUARIO VALIDO " + usuario.getNombre());
                    if (RU.getId_usr_status() == 0) {
                        respuesta.setResultado(3);
                        respuesta.setMensaje("Usuario Inactivo. Consulte con el Administrador");
                    } else if (RU.getId_usr_status() == 99) {
                        respuesta.setResultado(99);
                        respuesta.setMensaje("Modifique su Informacion por seguridad.|" + RU.getUsr_login());
                    } else if (RU.getId_usr_status() == 98) {
                        respuesta.setResultado(98);
                        respuesta.setMensaje("Modifique su clave de acceso por seguridad.|" + RU.getUsr_login());
                    } else {
                        respuesta.setResultado(1);
                        respuesta.setMensaje("Usuario Valido|" + RU.getId_usuario());
                    }
                } else {
                    respuesta.setResultado(2);
                    respuesta.setMensaje("Usuario Invalido. Verifique el usuario y/o password");
                }
            }
            resp = gson.toJson(respuesta);
            LOGGER.info("Respuesta usuarioLogin: " + resp);
            resp = Crypto.aesEncrypt(key, resp);
            return resp;
         }
        
        public boolean addUsuario(Usuario usuario) {
        boolean flag = false;
        String wPwd = "99" + U.random();
        UsuarioVO newusr = new UsuarioVO();
        newusr.setId_usuario(new java.util.Date().getTime());
        newusr.setUsr_login(usuario.getLogin());
        if(usuario.getStatus() != 1000){
            newusr.setUsr_pwd(crypto.Crypto.sha1(wPwd));
        }else{
            String kk= ParametroDAO.parsePass(wPwd);
            newusr.setUsr_pwd(crypto.Crypto.aesEncrypt(kk,  wPwd ));
        }
        newusr.setUsr_telefono(usuario.getTelefono());
        newusr.setUsr_nombre(usuario.getNombre());
        newusr.setUsr_direccion(usuario.getDireccion());
        newusr.setUsr_tdc_numero(ParametroDAO.setSMS(usuario.getTarjeta()));
        newusr.setUsr_tdc_vigencia(usuario.getVigencia());
        newusr.setId_banco(usuario.getBanco());
        newusr.setId_tipo_tarjeta(usuario.getTipotarjeta());
        newusr.setId_proveedor(usuario.getProveedor());
        if (usuario.getStatus() != 1000) {
                newusr.setId_usr_status(99); // obliga a cambiar pwd y correo
            } else {
                newusr.setId_usr_status(1); //Deja activado el usuario con los datos por default
            }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        if (usuario.getNacimiento() != null && !"".equals(usuario.getNacimiento())) {
           /* try { */
                newusr.setUsr_fecha_nac(java.sql.Date.valueOf(usuario.getNacimiento()));
            /*} catch (ParseException e) {
                LOGGER.info("Error al parsear : " + e.getMessage());
            }*/
        } else {
            newusr.setUsr_fecha_nac(null);
        }
        newusr.setUsr_apellido(usuario.getApellido());
        newusr.seteMail(usuario.getMail());
        newusr.setImei(usuario.getImei());
        newusr.setTipo(usuario.getTipo());
        newusr.setSoftware(usuario.getSoftware());
        newusr.setModelo(usuario.getModelo());
        newusr.setWkey(usuario.getKey());
        newusr.setTelefono_original(usuario.getTelefono());
        newusr.setUsr_materno(usuario.getMaterno());
        newusr.setUsr_sexo(usuario.getSexo());
        newusr.setUsr_tel_casa(usuario.getTel_casa());
        newusr.setUsr_tel_oficina(usuario.getTel_oficina());
        newusr.setUsr_id_estado(usuario.getId_estado());
        newusr.setUsr_ciudad(usuario.getCiudad());
        newusr.setUsr_calle(usuario.getCalle());
        newusr.setUsr_num_ext(usuario.getNum_ext());
        newusr.setUsr_num_interior(usuario.getNum_interior());
        newusr.setUsr_colonia(usuario.getColonia());
        newusr.setUsr_cp(usuario.getCp());
        newusr.setUsr_dom_amex(usuario.getDom_amex());
        newusr.setUsr_terminos(usuario.getTerminos());
        try {
            flag = addUser(newusr);
            String mensaje = null;
            try{
                if(usuario.getTipo_cliente()!=16){
                    mensaje = URLEncoder.encode("Hola, bienvenido a MobileCard " + usuario.getNombre() + " tu usuario es: " + usuario.getLogin() + "  tu contraseña es: " + wPwd,"UTF-8");
                } else {
                    mensaje = URLEncoder.encode("Bienvenido a TESORERIA GDF.\n Su nombre de usuario y contrasena son:\n Usuario: " + usuario.getLogin() + "\n Contrasena: " + wPwd,"UTF-8");
                }                    
            }catch(Exception e){
                LOGGER.debug("ERROR - generar cadena de SMS: " + e.getMessage());
                //e.printStackTrace();
                if(usuario.getTipo_cliente()!=16){
                    mensaje="Hola, bienvenido a MobileCard " + usuario.getNombre() + " tu usuario es: " + usuario.getLogin() + "  tu contraseña es: " + wPwd;
                } else {
                    mensaje = "Bienvenido a TESORERIA GDF.\n Su nombre de usuario y contraseña son:\n Usuario: " + usuario.getLogin() + "\n Contraseña: " + wPwd;
                }
            }
            LOGGER.debug("Fin del Registro.");
            if (usuario.getTipo().toUpperCase().equals("IPAD") || usuario.getTipo().toUpperCase().equals("IPOD")) {
                if(usuario.getTipo_cliente()!=16){
                    //new ParametroDAO().EnviaMail("", usuario.getMail(), consultaParametros("@MENSAJE_REGISTRO"), consultaParametros("@ASUNTO_REGISTRO"), usuario.getLogin(), wPwd, "", "", "", "",antadService);
                } else {
                    new ParametroDAO().EnviaMailBGDF(usuario.getLogin(), wPwd,usuario.getMail());
                }
            } else {
                /*          INICIO DE LA MODIFICACION
                 * SE IMPLEMENTO EL METODO DE EnviarSMS QUE UTILIZA
                 * EL ADDCELBATCH PARA VERIFICAR EN LA DB LOS SMS QUE 
                 * NO SE HAN ENVIADO. SE COMENTO EL METODO ANTERIO DE
                 * setSMS QUE ESTABA EN .NET (YA NO SE UTILIZA)
                 * 
                 * 25/07/13
                 * SE AGREGO EL ENVIO DE EMAIL A TODOS LOS DISPOSITOVOS 
                 * DESPUES DEL REGISTRO, PRIMERO ENVIA EL SMS Y LUEGO EL 
                 * MAIL EN CASO DE LOS QUE NO SON IPAD NI IPOD.
                 */
                //setSMS(usuario.getNombre(), usuario.getTelefono(), Mensaje);
                LOGGER.debug("Inicia envio de SMS...");
                if (utilsService.EnviarSMS(usuario.getTelefono(), mensaje )) {
                    LOGGER.debug("Se registro el envio del mensaje con exito para el numero " + usuario.getTelefono() + " usuario " + usuario.getNombre());
                } else {
                    LOGGER.debug("No se pudo registrar el envio del mensaje para el numero " + usuario.getTelefono() + " usuario " + usuario.getNombre());
                }
                if(usuario.getTipo_cliente()!=16){
                    LOGGER.debug("Entro a if antes de enviar el mail");
                    LOGGER.info("MAIL : "+usuario.getMail());
                    LOGGER.info("LOGIN : "+usuario.getLogin());
                    LOGGER.info("WPWD : "+wPwd);
                   // new ParametroDAO().EnviaMail("", usuario.getMail(), consultaParametros("@MENSAJE_REGISTRO"), consultaParametros("@ASUNTO_REGISTRO"), usuario.getLogin(), wPwd, "", "", "", "",antadService);
                } else {
                    new ParametroDAO().EnviaMailBGDF(usuario.getLogin(), wPwd,usuario.getMail());
                }
                /*
                 *          FIN DE LOS CAMBIOS
                 */
            }
            flag = true;
            LOGGER.debug("Cadena usuario.getLogin() " + usuario.getLogin());
            LOGGER.debug("Cadena usuario.getEtiqueta() " + usuario.getEtiqueta());
            LOGGER.debug("Cadena usuario.getNumero() " + usuario.getNumero());
            LOGGER.debug("Cadena usuario.getDv() " + usuario.getDv());
            if (!usuario.getEtiqueta().equals("") && !usuario.getNumero().equals("") && usuario.getDv() != 0) {
                UsuarioTagsVO usuariotag = new UsuarioTagsVO();
                UsuarioVO x = new UsuarioVO();
                x.setUsr_login(usuario.getLogin());
                x.setUsr_pwd(usuario.getPassword());

                LOGGER.debug("Cadena usuario.getLogin() " + usuario.getLogin());
                LOGGER.debug("Cadena x.getLogin() " + x.getUsr_login());

                x = obtenerUsuario(x);
                LOGGER.debug("Cadena x.getLogin() 2 " + x.getUsr_login());
                LOGGER.debug("Cadena x.getUsuario() 2 " + x.getId_usuario());

                usuariotag.setId_usuario(x.getId_usuario());
                usuariotag.setEtiqueta(usuario.getEtiqueta());
                usuariotag.setId_tiporecargatag(usuario.getIdTipoRecargaTag());
                usuariotag.setTag(usuario.getNumero());
                usuariotag.setDv(usuario.getDv());

                if (utilsService.addTag(usuariotag)) {
                    LOGGER.debug("Se registro el usuario TAG con EXITO: "  +x.getId_usuario() );
                } else {
                    LOGGER.debug("No se pudo registrar el usuario TAG: "  +x.getId_usuario() );
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error addUsuario {}",ex);
        } finally {
            LOGGER.error("Se ejecuto addUsuario {}");
        }
        return flag;
    }
        
        /**
	 * Nueva clase obtenerUsuario que sustituye a la clase mostrada arriba llamada getUsuario. 
	 * se construye con la nueva arquitectura de mybatis.
	 * @author: JCDP
	 * @version: 12/07/2016
	 */
	public UsuarioVO obtenerUsuario(UsuarioVO usuario) {
        UsuarioVO userRep = null;
        String wPwd = "";
        String key = "1234567890ABCDEF0123456789ABCDEF";
        try {
        	/*nuevo codigo*/
        	userRep = mapper.getUsuario(usuario.getUsr_login());
        	/*fin de nuevo codigo*/
            
//            logger.debug("CADENA USUARIO ESTATUS 1: " + userRep.getStatus());
            if (userRep != null && userRep.getId_usr_status() == 99) {
            	LOGGER.info("obtenerUsuario IF");
                wPwd = usuario.getPasswordS();
            } else {
            	LOGGER.info("obtenerUsuario ELSE");
                wPwd = Crypto.aesEncrypt(key, usuario.getUsr_pwd());
            }
            
            if(userRep != null && userRep.getUsr_pwd() != null && Crypto.aesEncrypt(key, usuario.getUsr_pwd()).equals(wPwd)){
            	LOGGER.info("obtenerUsuario IF ALTERNATIVO");
                usuario = userRep;
            }
        } catch (Exception ex) {
        	LOGGER.error("Ocurrio un error en metodo obtenerUsuario(): {}",ex);
        } finally {
        	LOGGER.info(LOG_RESPUESTA_OBTENER_USUARIO+usuario.getUsr_nombre()); 
        }
        return usuario;
    }
        
         public String usuarioPasswordUpdateService(String jsonEnc){
             LOGGER.info("**********************************  PASSWORD UPDATE");
             String key = "1234567890ABCDEF0123456789ABCDEF";
             String resp = null;
             Respuesta respuesta = new Respuesta();
             UserPasswordUpdate userPasswordUpdate = new UserPasswordUpdate();
             String json;
             if (jsonEnc == null) {
                 LOGGER.info("parameter json null ");
             }
             if (jsonEnc.equals("")) {
                 json = (String) jsonEnc.toString();
                 LOGGER.info("Dentro de If en insertaUsuario ");
             } else {
                 json = (String) jsonEnc.toString();
                 LOGGER.info("Dentro de else en insertaUsuario ");
             }
             LOGGER.info("CADENA json update password: " + json);
             String[] Cad = new com.addcel.iave.integration.ParametroDAO().Asf(json);
             key = Cad[0];
             json = Cad[1];

             Type collectionType = new TypeToken<UserPasswordUpdate>() {
             }.getType();
             userPasswordUpdate = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);
             UsuarioVO usuario = new UsuarioVO();
             usuario.setUsr_login(userPasswordUpdate.getLogin());

             usuario.setUsr_pwd(Crypto.aesEncrypt(key, userPasswordUpdate.getPassword()));
             String key2 = new com.addcel.iave.integration.ParametroDAO().parsePass(userPasswordUpdate.getNewPassword());
             String us = usuario.getUsr_login();
             String p = userPasswordUpdate.getNewPassword();

             userPasswordUpdate.setNewPassword(Crypto.aesEncrypt(key2, userPasswordUpdate.getNewPassword()));

             usuario = consultaUsuarioById(usuario.getUsr_login());

             LOGGER.info("CADENA LogIn " + us);

             if (p.length() < 8) {
                 respuesta.setResultado(3);
                 respuesta.setMensaje("La nueva contraseña debe tener por lo menos 8 caracteres.");
             } else {
                 if (newValidateUsuario(usuario)) {

                     //if (updatePassword(usuario, antadService)) {
                    if (updatePassword(usuario)) {
                         respuesta.setResultado(1);
                         respuesta.setMensaje("Password actualizado");
                     } else {
                         respuesta.setResultado(2);
                         respuesta.setMensaje("Ha ocurrido un error, intentelo nuevamente");
                     }
                 } else {
                     respuesta.setResultado(2);
                     respuesta.setMensaje("Error en Password");
                 }
             }
             resp = gson.toJson(respuesta);
             LOGGER.info("Respuesta updatePassword: " + resp);
             resp = Crypto.aesEncrypt(key, resp);
             return resp;
         }
        
         /**
	 * Nueva clase newValidateUsuario que sustituye a la clase mostrada arriba llamada validateUsuario. 
	 * se construye con la nueva arquitectura de persistencia mybatis.
	 * @author: JCDP
	 * @version: 12/07/2016
	 */
            public boolean newValidateUsuario(UsuarioVO usuario) {
            boolean flag = false;
            UsuarioVO vusuario = null;
            try {
                vusuario = mapper.validaUsuario(usuario.getUsr_login(), usuario.getUsr_pwd());
                if (vusuario != null) {
                    flag = true;
                }
            } catch (Exception ex) {
                LOGGER.error("Ocurrio un error en método newValidateUsuario(UsuarioVO) : {}", ex);
            } finally {
                //closeConPrepStResSet(statement, resultSet);
            }
            return flag;
        }
         
        /*
	 * metodo para actualizar password de usuario 
	 * by JCDP
	 * Fecha : 18/07/2016
	 * */
	//public boolean updatePassword(long usuario, String password, String sMail, String u, String p) throws SQLException, ClassNotFoundException {
	//public boolean updatePassword(UsuarioVO usuario, TagServiciosService service){
        public boolean updatePassword(UsuarioVO usuario){    
        boolean flag = false;
        try {
        	mapper.updateUsuarios(usuario);
        	flag = true;
            //TagServiciosController.sendMail("", usuario.geteMail(), "@MENSAJE_RECOVERYPWD", "@ASUNTO_RECOVERYPWD", usuario.getUsr_login(), usuario.getUsr_pwd(), "", "", "", "", service);
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error en nuevo metodo updatePassword() : {}",ex);
        } 
        return flag;
    }        
            
         public String usuarioUpdateService(String jsonEnc){
             String resp = null;
             String key = "1234567890ABCDEF0123456789ABCDEF";
             Respuesta respuesta = new Respuesta();
             Usuario usuario = new Usuario();
             String json;
             if (jsonEnc == null) {
                 LOGGER.info("parameter json null ");
             }
             if (jsonEnc.equals("")) {
                 json = (String) jsonEnc.toString();
                 LOGGER.info("Dentro de If en usuarioUpdateService ");
             } else {
                 json = (String) jsonEnc.toString();
                 LOGGER.info("Dentro de else en usuarioUpdateService ");
             }
             LOGGER.info("*********************     USUARIO Update... ");

             LOGGER.info("CADENA userUpdate: " + json);

             String[] Cad = new com.addcel.iave.integration.ParametroDAO().Asf(json);
             key = Cad[0];
             json = Cad[1];
             Type collectionType = new TypeToken<Usuario>() {
             }.getType();
             usuario = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);

             usuario.setPassword(Crypto.aesEncrypt(key, usuario.getPassword()));

             LOGGER.info("CADENA LOGIN " + usuario.getLogin());
             LOGGER.info("CADENA IDUSUARIO " + usuario.getUsuario());

             UsuarioVO userup = new UsuarioVO();
             userup.setId_usuario(usuario.getUsuario());
             userup.setUsr_login(usuario.getLogin());
             userup.setUsr_pwd(usuario.getPassword());
             userup.setUsr_telefono(usuario.getTelefono());
             userup.setUsr_nombre(usuario.getNombre());
             userup.setUsr_direccion(usuario.getDireccion());
             userup.setUsr_tdc_numero(ParametroDAO.setSMS(usuario.getTarjeta()));
             userup.setUsr_tdc_vigencia(usuario.getVigencia());
             userup.setId_banco(usuario.getBanco());
             userup.setId_tipo_tarjeta(usuario.getTipotarjeta());
             userup.setId_proveedor(usuario.getProveedor());
             userup.setId_usr_status(usuario.getStatus()); // obliga a cambiar pwd y correo
             userup.setUsr_fecha_nac(java.sql.Date.valueOf(usuario.getNacimiento()));
             userup.setUsr_apellido(usuario.getApellido());
             userup.seteMail(usuario.getMail());
             userup.setImei(usuario.getImei());
             userup.setTipo(usuario.getTipo());
             userup.setSoftware(usuario.getSoftware());
             userup.setModelo(usuario.getModelo());
             userup.setWkey(usuario.getKey());
             userup.setTelefono_original(usuario.getTelefono());
             userup.setUsr_materno(usuario.getMaterno());
             userup.setUsr_sexo(usuario.getSexo());
             userup.setUsr_tel_casa(usuario.getTel_casa());
             userup.setUsr_tel_oficina(usuario.getTel_oficina());
             userup.setUsr_id_estado(usuario.getId_estado());
             userup.setUsr_ciudad(usuario.getCiudad());
             userup.setUsr_calle(usuario.getCalle());
             userup.setUsr_num_ext(usuario.getNum_ext());
             userup.setUsr_num_interior(usuario.getNum_interior());
             userup.setUsr_colonia(usuario.getColonia());
             userup.setUsr_cp(usuario.getCp());
             userup.setUsr_dom_amex(usuario.getDom_amex());
             userup.setUsr_terminos(usuario.getTerminos());

             if (utilsService.ExisteMailUp(usuario.getMail(), usuario.getLogin())) {
                 respuesta.setResultado(3);
                 respuesta.setMensaje("El Correo Electronico ya fue asignado a otro usuario. Intente de nuevo.");
             } else if (utilsService.ExisteTUp(usuario.getTarjeta(), usuario.getLogin())) {
                 respuesta.setResultado(4);
                 respuesta.setMensaje("La tarjeta proporcionada ya esta asignada a otro usuario. Intente de nuevo.");
             } else if (update(userup)) {
                 respuesta.setResultado(1);
                 respuesta.setMensaje("El registro ha sido actualizado satisfactoriamente");
             } else {
                 respuesta.setResultado(2);
                 respuesta.setMensaje("Ha ocurrido un error, intentelo nuevamente");
             }
             resp = gson.toJson(respuesta);
             LOGGER.info("Respuesta updatePassword: " + resp);
             resp = Crypto.aesEncrypt(key, resp);
             
             return resp;
         }
         
         /*
	 * metodo para realizar un update a un usuario se encuentra activo
	 * by JCDP
	 * Fecha : 19/07/2016
	 * */
        public boolean update(UsuarioVO usuario) {
	        boolean flag = false;
	        try {
	        	if (UsuarioActivo(usuario)) {
	                mapper.updateUsuarios(usuario);
	                flag = true;
                        LOGGER.error("Actualizacion de usuario exitosa(): {}");
	            } else {
	                flag = false;
                        LOGGER.error("El usuario no se encuentra activo no se pudo actualizar");
	            }
	        } catch (Exception ex) {
	            LOGGER.error("Ocurrio un error en metodo update(): {}",ex);
	        } 
	        return flag;
	    }
         
        /*
	 * metodo para consultar si un usuario se encuentra activo
	 * by JCDP
	 * Fecha : 19/07/2016
	 * */
	public boolean UsuarioActivo(UsuarioVO usuario) {
        boolean flag = false;
        UsuarioVO user = new UsuarioVO();
        try {
            user = mapper.usuarioActivo(usuario.getUsr_login());
            if (user != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error en metodo UsuarioActivo: {}", ex);
        }
        return flag;
    }
        
         public String getUserDataService(String jsonEnc){
             String key = "1234567890ABCDEF0123456789ABCDEF";
    	        UsuarioVO usuario = new UsuarioVO();
             String json;
             if (jsonEnc == null) {
                 LOGGER.info("parameter json null ");
             }
             if (jsonEnc.equals("")) {
                 json = (String) jsonEnc.toString();
                 //LOGGER.info("Dentro de If  ");
             } else {
                 json = (String) jsonEnc.toString();
                 //LOGGER.info("Dentro de else ");
             }
             LOGGER.info("CADENA getUserData " + json);
             String[] Cad = new com.addcel.iave.integration.ParametroDAO().Asf(json);
             key = Cad[0];
             json = Cad[1];
             Type collectionType = new TypeToken<UsuarioVO>() {
             }.getType();
             usuario = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);

             usuario.setUsr_pwd(Crypto.aesEncrypt(key, usuario.getUsr_pwd()));

             usuario = obtenerUsuario(usuario);
             String jsonOutput = gson.toJson(usuario);
             return Crypto.aesEncrypt(key, jsonOutput);
         }
         
         public String getProductosService(String jsonEnc){
             String key = "1234567890ABCDEF0123456789ABCDEF";
             Producto producto = new Producto();
             String json;
             if (jsonEnc == null) {
                 LOGGER.info("parameter json null ");
             }
             if (jsonEnc.equals("")) {
                 json = (String) jsonEnc.toString();
             } else {
                 json = (String) jsonEnc.toString();
                 LOGGER.info("Dentro de else en end point adc_getPoducts " + json);
             }
             Type collectionType = new TypeToken<Producto>() {
             }.getType();
             producto = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);
             LOGGER.info("id_producto " + producto.getClave());
              LOGGER.info("pro_clave " + producto.getClaveWS());
              LOGGER.info("pro_monto " + producto.getMonto());
              LOGGER.info("pro_path" + producto.getPath());
              LOGGER.info("id_proveedor " + producto.getProveedor());
              LOGGER.info("nombre " + producto.getNombre());
             ProveedorVO prov = new ProveedorVO();
             prov = utilsService.consultaProveedorByClaveWS(producto.getProveedor());
             Contenedor contenedor = new Contenedor();
             contenedor.setProductos(utilsService.getProductosByProveedor(prov.getId_proveedor()));
             LOGGER.info("llega 5 ");
             String jsonOutput = gson.toJson(contenedor);
             return jsonOutput;
         }
         
         /*************************************************************************************************************/
         
        public boolean existeUsuario(Usuario usuario) {
        boolean flag = false;
        UsuarioVO usuarioaux = new UsuarioVO();
        try {
            usuarioaux = consultaUsuarioById(usuario.getLogin());
            if (usuarioaux != null) {
                LOGGER.error("Usuario Existe");
                flag = true;
            }
            if(!flag){
                LOGGER.error("Usuario No Existe");
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error en metodo existeUsuario: {}",ex);
        } finally {
            LOGGER.error("Se ejecuto metodo existeUsuario: {}");
        }
        return flag;
    }
        
        /*****************************************/
	/*nuevo metodo de consulta de usuario por usr_login */
	/*****************************************/
	public UsuarioVO consultaUsuarioById(String usr_login) throws java.lang.NullPointerException {
		UsuarioVO usuario = new UsuarioVO();
		String  parametro = "";
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO + usr_login);
			usuario = mapper.getUsuario(usr_login);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO+"["+e.getCause()+"]");
			usr_login = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO+usr_login);
		}
		return usuario;
	}
        
        /*****************************************/
	/*nuevo metodo de consulta de usuario por email */
	/*****************************************/
	public UsuarioVO consultaUsuarioByEmail(String mail) {
		UsuarioVO usuario = new UsuarioVO();
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO_EMAIL + mail);
			usuario = mapper.getUsuarioByEmail(mail);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO_EMAIL+"["+e.getCause()+"]");
			mail = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO_EMAIL+mail);
		}
		return usuario;
	}
        
        /*****************************************/
	/*nuevo metodo de consulta de usuario por email */
	/*****************************************/
	public UsuarioVO consultaUsuarioByT(String tdc) {
		UsuarioVO usuario = new UsuarioVO();
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO_TDC + tdc);
			usuario = mapper.getUsuarioByT(tdc);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO_TDC+"["+e.getCause()+"]");
			tdc = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO_TDC+tdc);
		}
		return usuario;
	}
	
        /*****************************************/
	/*nuevo metodo de consulta de usuario por imei */
	/*****************************************/
	public UsuarioVO consultaUsuarioByImei(String imei) {
		UsuarioVO usuario = new UsuarioVO();
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO_IMEI + imei);
			usuario = mapper.getUsuarioByImei(imei);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO_IMEI+"["+e.getCause()+"]");
			imei = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO_IMEI+imei);
		}
		return usuario;
	}
        
        /*****************************************/
	/*nuevo metodo de consulta de usuario por telefono */
	/*****************************************/
	public UsuarioVO consultaUsuarioByTelefono(String telefono) {
		UsuarioVO usuario = new UsuarioVO();
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO_TELEFONO + telefono);
			usuario = mapper.getUsuarioByTelefono(telefono);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO_TELEFONO+"["+e.getCause()+"]");
			telefono = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO_TELEFONO+telefono);
		}
		return usuario;
	}
        
        /*****************************************/
	/*nuevo metodo para agregar un nuevo usuario a t_usuarios o a t_usuario_tags */
	/*****************************************/
	public boolean addUser(UsuarioVO usuario) {
            boolean exito = false;
		try {
                    String wPwd = "99" + U.random();
                    LOGGER.info(LOG_SERVICE + LOG_PROCESO_ADD_USUARIO + usuario);
                    usuario.setId_usuario(new java.util.Date().getTime());
                    if (usuario.getId_usr_status() != 1000) {
                        usuario.setUsr_pwd(crypto.Crypto.sha1(wPwd));
                    } else {
                        String kk = ParametroDAO.parsePass(wPwd);
                        usuario.setUsr_pwd(crypto.Crypto.aesEncrypt(kk, wPwd));
                    }
                    usuario.setUsr_tdc_numero(ParametroDAO.setSMS(usuario.getUsr_tdc_numero()));
                    if (usuario.getId_usr_status() != 1000) {
                        usuario.setId_usr_status(99); // obliga a cambiar pwd y correo
                    } else {
                        usuario.setId_usr_status(1); //Deja activado el usuario con los datos por default
                    }
                    if(usuario.getUsr_fecha_nac() != null && !"".equals(usuario.getUsr_fecha_nac())){
            	           usuario.setUsr_fecha_nac(usuario.getUsr_fecha_nac());
                    } else {
                        usuario.setUsr_fecha_nac(null);
                    }
			mapper.insertUsuarios(usuario);
                        String mensaje = null;
                try {
                    if (usuario.getTipo_cliente() != 16) {
                        mensaje = URLEncoder.encode("Hola, bienvenido a MobileCard " + usuario.getUsr_nombre() + " tu usuario es: " + usuario.getUsr_login() + "  tu contraseña es: " + wPwd, "UTF-8");
                    } else {
                        mensaje = URLEncoder.encode("Bienvenido a TESORERIA GDF.\n Su nombre de usuario y contrasena son:\n Usuario: " + usuario.getUsr_login() + "\n Contrasena: " + wPwd, "UTF-8");
                    }
                } catch (Exception e) {
                    LOGGER.debug("ERROR - generar cadena de SMS: " + e.getMessage());
                    //e.printStackTrace();
                    if (usuario.getTipo_cliente() != 16) {
                        mensaje = "Hola, bienvenido a MobileCard " + usuario.getUsr_nombre() + " tu usuario es: " + usuario.getUsr_login() + "  tu contraseña es: " + wPwd;
                    } else {
                        mensaje = "Bienvenido a TESORERIA GDF.\n Su nombre de usuario y contraseña son:\n Usuario: " + usuario.getUsr_login() + "\n Contraseña: " + wPwd;
                    }
                }
                    LOGGER.debug("Fin del Registro.");
                    if (usuario.getTipo().toUpperCase().equals("IPAD") || usuario.getTipo().toUpperCase().equals("IPOD")) {
                        if (usuario.getTipo_cliente() != 16) {
                            //AntadServiciosController.sendMail("", usuario.geteMail(), "@MENSAJE_REGISTRO", "@ASUNTO_REGISTRO", usuario.getUsr_login(), wPwd, "", "", "", "",mapper);
                        } else {
                            new ParametroDAO().EnviaMailBGDF(usuario.getUsr_login(), wPwd, usuario.geteMail());
                        }
                    } else {
                        /*          INICIO DE LA MODIFICACION
                         * SE IMPLEMENTO EL METODO DE EnviarSMS QUE UTILIZA
                         * EL ADDCELBATCH PARA VERIFICAR EN LA DB LOS SMS QUE 
                         * NO SE HAN ENVIADO. SE COMENTO EL METODO ANTERIO DE
                         * setSMS QUE ESTABA EN .NET (YA NO SE UTILIZA)
                         * 
                         * 25/07/13
                         * SE AGREGO EL ENVIO DE EMAIL A TODOS LOS DISPOSITOVOS 
                         * DESPUES DEL REGISTRO, PRIMERO ENVIA EL SMS Y LUEGO EL 
                         * MAIL EN CASO DE LOS QUE NO SON IPAD NI IPOD.
                         */
                        //setSMS(usuario.getNombre(), usuario.getTelefono(), Mensaje);
                        LOGGER.debug("Inicia envio de SMS...");
                        if (utilsService.EnviarSMS(usuario.getUsr_telefono(), mensaje)) {
                            LOGGER.debug("Se registro el envio del mensaje con exito para el numero " + usuario.getUsr_telefono() + " usuario " + usuario.getUsr_nombre());
                        } else {
                            LOGGER.debug("No se pudo registrar el envio del mensaje para el numero " + usuario.getUsr_telefono() + " usuario " + usuario.getUsr_nombre());
                        }
                        if (usuario.getTipo_cliente() != 16) {
                           // new ParametroDAO().EnviaMail("", usuario.geteMail(), "@MENSAJE_REGISTRO", "@ASUNTO_REGISTRO", usuario.getUsr_login(), wPwd, "", "", "", "");
                        } else {
                            new ParametroDAO().EnviaMailBGDF(usuario.getUsr_login(), wPwd, usuario.geteMail());
                        }
                        /*
                         *          FIN DE LOS CAMBIOS
                         */
                    }
                    exito = true;
                    if (!usuario.getEtiqueta().equals("") && !usuario.getNumero().equals("") && usuario.getDv() != 0) {
                        UsuarioTagsVO usuariotag = new UsuarioTagsVO();
                        UsuarioVO x = new UsuarioVO();
                        x.setUsr_login(usuario.getUsr_login());
                        x.setUsr_pwd(usuario.getUsr_pwd());

                        LOGGER.debug("Cadena usuario.getLogin() " + usuario.getUsr_login());
                        LOGGER.debug("Cadena x.getLogin() " + x.getUsr_login());

                        x = mapper.getUsuario(x.getUsr_login());
                        LOGGER.debug("Cadena x.getLogin() 2 " + x.getUsr_login());
                        LOGGER.debug("Cadena x.getUsuario() 2 " + x.getId_usuario());

                        usuariotag.setId_usuario(x.getId_usuario());
                        usuariotag.setEtiqueta(usuario.getEtiqueta());
                        usuariotag.setId_tiporecargatag(usuario.getIdtiporecargatag());
                        usuariotag.setTag(usuario.getNumero());
                        usuariotag.setDv(usuario.getDv());

                        if (utilsService.addTag(usuariotag)) {
                            LOGGER.debug("Se registro el usuario TAG con EXITO: " + x.getId_usuario());
                        } else {
                            LOGGER.debug("No se pudo registrar el usuario TAG: " + x.getId_usuario());
                        }
                    }
            } catch (Exception e) {
                LOGGER.error(LOG_SERVICE + LOG_ERROR_AGREGAR_USUARIO + "[" + e.getCause() + "]");
            } finally {
                LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO_TELEFONO + usuario);
            }
		return exito;
	}
}
