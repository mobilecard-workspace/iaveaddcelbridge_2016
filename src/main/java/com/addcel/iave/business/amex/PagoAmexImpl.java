/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.iave.business.amex;

import com.addcel.iave.utils.ConstantesAmex;
import com.google.gson.Gson;
import com.addcel.iave.model.vo.RespuestaAmex;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author JCDP
 */
public class PagoAmexImpl implements PagoAmex {
    
    private static final Logger logger = LoggerFactory.getLogger(PagoAmexImpl.class);

    public RespuestaAmex realizarPago(String tarjeta, String vigencia, String monto, String cvv2, String direccion, String codigoPostal) {
        
        RespuestaAmex respAmex = new RespuestaAmex();
        
        try {
            if(vigencia!=null&&vigencia.matches("\\d{2}/\\d{2}")){
                vigencia=vigencia.replace("/", "");
            }
            String data = "tarjeta=" + URLEncoder.encode(tarjeta, "UTF-8") +
                    "&vigencia=" + URLEncoder.encode(vigencia, "UTF-8") +
                    "&monto=" + URLEncoder.encode(monto, "UTF-8") +
                    "&cid=" + URLEncoder.encode(cvv2, "UTF-8") +
                    "&direccion=" + URLEncoder.encode(direccion, "UTF-8") +
                    "&cp=" + URLEncoder.encode(codigoPostal, "UTF-8");
            
            //Envío de datos
            logger.debug("Envío de datos AMEX: " + data);
            URL url = new URL(ConstantesAmex.URL_AMEX);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            //logger.debug("Datos enviados, esperando respuesta AMEX");
            // Recepción de respuesta
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            Gson gson = new Gson();
            //logger.debug("Recepción de datos AMEX");
            while ((line = rd.readLine()) != null) {
                logger.debug("Respuesta AMEX: " + line);
                respAmex = gson.fromJson(line, RespuestaAmex.class);
            }
            wr.close();
            rd.close();
        } catch (NullPointerException e) {
            logger.error("Error: ", e);
        } catch (UnsupportedEncodingException e) {
            logger.error("Error: ", e);
        } catch (MalformedURLException e) {
            logger.error("Error: ", e);
        } catch (IOException e) {
            logger.error("Error: ", e);
        }
        return respAmex;
    }
    
    public RespuestaAmex realizarPagoTelepeaje(String tarjeta, String vigencia, String monto, String cvv2, String direccion, String codigoPostal, String producto) {
        
        RespuestaAmex respAmex = new RespuestaAmex();
        
        try {
            if(vigencia!=null && vigencia.matches("\\d{2}/\\d{2}")){
                vigencia=vigencia.replace("/", "");
            }
            String data = "tarjeta=" + URLEncoder.encode(tarjeta, "UTF-8") +
                    "&vigencia=" + URLEncoder.encode(vigencia, "UTF-8") +
                    "&monto=" + URLEncoder.encode(monto, "UTF-8") +
                    "&cid=" + URLEncoder.encode(cvv2, "UTF-8") +
                    "&direccion=" + URLEncoder.encode(direccion, "UTF-8") +
                    "&cp=" + URLEncoder.encode(codigoPostal, "UTF-8");
            
            if (producto.equals("IAVE")) {
                data = data + "&producto=IAVE";
            } else {
                if (producto.equals("OHL")) {
                    data = data + "&producto=OHL";
                } else {
                    if (producto.equals("VIAPASS")) {
                        data = data + "&producto=VIAPASS";
                    }
                }
            }
            
            //Envío de datos
            logger.debug("Envío de datos AMEX: " + data);
            URL url = new URL(ConstantesAmex.URL_AMEX);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            //logger.debug("Datos enviados, esperando respuesta AMEX");
            // Recepción de respuesta
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            Gson gson = new Gson();
            //logger.debug("Recepción de datos AMEX");
            while ((line = rd.readLine()) != null) {
                logger.debug("Respuesta AMEX: " + line);
                respAmex = gson.fromJson(line, RespuestaAmex.class);
            }
            wr.close();
            rd.close();
        } catch (NullPointerException e) {
            logger.error("Error: ", e);
        } catch (UnsupportedEncodingException e) {
            logger.error("Error: ", e);
        } catch (MalformedURLException e) {
            logger.error("Error: ", e);
        } catch (IOException e) {
            logger.error("Error: ", e);
        }
        return respAmex;
    }
}
